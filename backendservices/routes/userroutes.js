const exp=require('express');
//importing database object
const initdb=require('../DBConfig').initdb
const getdb=require('../DBConfig').getdb
const secret='secret'
//importing jwt
const jwt=require('jsonwebtoken')
//intailizing dbo
initdb();
//importing bcrypt
const bcrypt=require('bcrypt')
var userRoutes=exp.Router();
//import twilio
const accountSid = 'AC86e4d1234741f33ea67a865d168c5114';
const authToken = '8912b83dd768cee6fc97eef1812b717a';
const client = require('twilio')(accountSid, authToken);
//import nodemailier
const nodemailer=require('nodemailer');


// //userRoutes handler
// userRoutes.post('/register',(req,res,next)=>{
//     console.log(req.body)
//     //hashing the password using 
//     bcrypt.hash(req.body.password,5,(err,hashedPassword)=>{
//         if(err)
//         {
//             console.log(err)
//         }
//         else{
//             req.body.password=hashedPassword
//             console.log(req.body)
//             var dbo=getdb();
//             if(req.body.usertype==='owner')
//             {
//                 dbo.collection("owner").find({name:{$eq:req.body.name}}).toArray((err,dataArray)=>{
//                     if (dataArray.length==0)
//                     {
//                         dbo.collection("owner").insertOne(req.body,(err,success)=>{
//                             if(err){
//                                next(err)
//                             }
//                             else{
//                                 res.json({message:"registered successfully"})
//                             }
//                         })
//                     }
//                     else{
//                         res.json({message:"name exists"})
//                     }
//                 })
    
               
//             }
//             else{
//                 dbo.collection("vendor").find({name:{$eq:req.body.name}}).toArray((err,dataArray)=>{
//                     if (dataArray.length==0)
//                     {
//                         dbo.collection("vendor").insertOne(req.body,(err,success)=>{
//                             if(err){
//                                next(err)
//                             }
//                             else{
//                                 res.json({message:"registered successfully"})
//                             }
//                         })
//                     }
//                     else{
//                         res.json({message:"name exists"})
//                     }
//                 })
//             }
//         }
//     })
   
        
// })
//userRoutes handler patient registration
userRoutes.post('/register/patient',(req,res,next)=>{
    console.log(req.body)
    //hashing the password using 
    bcrypt.hash(req.body.password,5,(err,hashedPassword)=>{
        if(err)
        {
            console.log(err)
        }
        else{
            let transporter=nodemailer.
  createTransport({
      service:"gmail",
      auth:{
          user:"kalyanireddy678@gmail.com",
          pass:"xxxxxxxxxx"
      }
  });
  let info= transporter.sendMail({
      //sender address
      from:'"login details" <kalyanireddy678@gmail.com>',
      //list of recivers
      to:req.body.email,
      subject:"Patient credentials",//subject line
      text:`username: ${req.body.name},password: ${req.body.password}`,
  });




            req.body.password=hashedPassword
            console.log(req.body)
            var dbo=getdb();
            if(req.body.name==""||req.body.puname==""||req.body.password==""||req.body.mobilenumber==""||req.body.date==""){
                res.json({"message":"null values not inserted"});
            }else{
            dbo.collection("patientcollection").find({name:{$eq:req.body.name}}).toArray((err,dataArray)=>{
                if (dataArray.length==0)
                {
                    dbo.collection("patientcollection").insertOne(req.body,(err,success)=>{
                        if(err){
                           next(err)
                        }
                        else{
                            res.json({"message":"registered successfully"})
                        }
                    })
                }
                else{
                    res.json({"message":"duplicate Username"})
                }
            
            })
        }
    }
    })
   
        
})

//userRoutes handler doctor registration
userRoutes.post('/register/doctor',(req,res,next)=>{
    console.log(req.body)
    //hashing the password using 
    bcrypt.hash(req.body.password,5,(err,hashedPassword)=>{
        if(err)
        {
            console.log(err)
        }
        else{
            let transporter=nodemailer.
  createTransport({
      service:"gmail",
      auth:{
          user:"kalyanireddy678@gmail.com",
          pass:"xxxxxxxxxx"
      }
  });
  let info= transporter.sendMail({
      //sender address
      from:'"login details" <kalyanireddy678@gmail.com>',
      //list of recivers
      to:req.body.email,
      subject:"Doctor credentials",//subject line
      text:`username: ${req.body.name},password: ${req.body.password}`,//plain text body
      //html:"<b>hiii ra praveen</b>"//htmlbody
  });


            req.body.password=hashedPassword
            console.log(req.body)
            var dbo=getdb();
            if(req.body.name==""||req.body.duname==""||req.body.password==""||req.body.mobilenumber==""||req.body.timing==""||req.body.fee==""||req.body.experience==""||req.body.specilization==""||req.body.area==""){
                res.json({"message":"null values not inserted"});
            }else{
            dbo.collection("doctorcollection").find({name:{$eq:req.body.name}}).toArray((err,dataArray)=>{
                if (dataArray.length==0)
                {
                    dbo.collection("doctorcollection").insertOne(req.body,(err,success)=>{
                        if(err){
                           next(err)
                        }
                        else{
                            res.json({"message":"registered successfully"})
                        }
                    })
                }
                else{
                    res.json({message:"Duplicate Username"})
                }
            })
        }
    }
    })
 
        
})

//login validation user
userRoutes.post('/login',(req,res,next)=>{
    console.log(req.body)
    var dbo=getdb();
        if(req.body.usertype==='patient')
        {

            dbo.collection("patientcollection").find({name:{$eq:req.body.name}}).toArray((err,data)=>{
                if(err){
                   next(err)
                }
                else{
                    if (data.length==0)
                    {
                        res.json({message:'patient name invalid'})
                    }
                
                   else {
                    bcrypt.compare(req.body.password,data[0].password,(err,result)=>{
                            if (result==true)
                            {
                                //intailizing varaible
                                currentUserName=data[0].name
                                //create and send JSON token
                                const signedToken=jwt.sign({name:data[0].name},secret,{expiresIn: "1h"})
                                res.json({message:'patient logged in successfully',userdata:data,token:signedToken})
                            }
                            else{
                                res.json({message:'patient password invalid'})
                            }
                    })
                      
                   }
                }
            })
        }
        else{
            dbo.collection("doctorcollection").find({name:{$eq:req.body.name}}).toArray((err,data)=>{
                if(err){
                   next(err)
                }
                else{
                    if (data.length==0)
                    {
                        res.json({message:'doctor name invalid'})
                    }
                    
                   else {
                    bcrypt.compare(req.body.password,data[0].password,(err,result)=>{
                        if (result==true)
                        {
                            //intailizing varaible
                            currentUserName=data[0].name
                            //create and send JSON token
                            const signedToken=jwt.sign({name:data[0].name},secret,{expiresIn: "1h"})
                            res.json({message:'doctor logged in successfully',userdata:data,token:signedToken})
                        }
                        else{
                            res.json({message:'doctor password invalid'})
                        }
                    })
                      
                   }
                }
            })
        }
        
})

userRoutes.get('/doctors',(req,res)=>{
    dbo=getdb();
    dbo.collection('doctorcollection').find().toArray((err,dataArray)=>{
        if(err){
            console.log('error in saving data')
            console.log(err)
        }
        else{
                    res.json({"message":dataArray})
                    console.log("dataArray:",dataArray);
                }
    })
})

//forgot password//
userRoutes.post('/forgotpassword',(req,res,next)=>{
    console.log(req.body)
    var dbo=getdb()
    if(req.body.usertype=="doctor"){
        doc="doctorcollection"
    }
    else{
      doc="patientcollection"
    }
    dbo.collection(doc).find({name:req.body.name}).toArray((err,userArray)=>{
        if(err){
            next(err)
        }
        else{
            if(userArray.length===0){
                res.json({message:"user not found"})
            }
            else{

                jwt.sign({name:userArray[0].name},secret,{expiresIn:3600},(err,token)=>{
                    if(err){
                     next(err);
                    }
                    else{
                        var OTP=Math.floor(Math.random()*99999)+11111;
                        console.log(OTP)
                        
                        client.messages.create({
                            body: OTP,
                            from: '+12054309838', // From a valid Twilio number
                            to: '+9194940 98906',  // Text this number
  
                        })
                        .then((message) => {
                            dbo.collection('OTPCollection').insertOne({
                                OTP:OTP,
                                name:userArray[0].name,
                                OTPGeneratedTime:new Date().getTime()+15000
                        },(err,success)=>{
                            if(err){
                                next(err)
                            }
                            else{                                        
                                res.json({"message":"user found",
                                    "token":token,
                                    "OTP":OTP,
                                    "name":userArray[0].name
                                })
                            }
                        })
                        });

                    }
                    
                })
            }
        }
    })
})

//verify OTP
userRoutes.post('/otp',(req,res,next)=>{
    console.log(req.body)
    console.log(new Date().getTime())
    var dbo=getdb()
    var currentTime=new Date().getTime()
    dbo.collection('OTPCollection').find({"OTP":req.body.OTP}).toArray((err,OTPArray)=>{
        if(err){
            next(err)
        }
        else if(OTPArray.length===0){
            res.json({"message":"invalidOTP"})
        }
        else if(OTPArray[0].OTPGeneratedTime < req.body.currentTime){
            res.json({"message":"invalidOTP"})
        }
        else{
            
            dbo.collection('OTPCollection').deleteOne({OTP:req.body.OTP},(err,success)=>{
                if(err){
                    next(err);
                }
                else{
                    console.log(OTPArray)
                    res.json({"message":"verifiedOTP"})
                }
            })
        }
    })
})

//changing password
userRoutes.put('/changepassword',(req,res,next)=>{
    console.log(req.body)
    var dbo=getdb()
    bcrypt.hash(req.body.password,6,(err,hashedPassword)=>{
        if (err) {
            next(err)
        } else {
            console.log(hashedPassword)
            if(req.body.usertype=="doctor")
            {
                doc="doctorcollection"
            }
            else{
                doc="patientcollection"
            }
            dbo.collection(doc).updateOne({name:req.body.name},{$set:{
                password:hashedPassword
            }},(err,success)=>{
                if(err){
                    next(err)
                }
                else{
                    res.json({"message":"password changed"})
                }
            }) 
        }
    })
    
})
//error handling callback function
userRoutes.use((err,req,res,next)=>{
    console.log(err)
})
module.exports=userRoutes