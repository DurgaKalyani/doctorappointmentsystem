(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/aboutus/aboutus.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/aboutus/aboutus.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5 mb-5\">\n    <div class=\"row\">\n        <div class=\"col-md-8\">\n            <h1 class=\"text-center text-info\">About Us</h1>\n        \n            <p>Doctors make people healthier. When people get sick, doctors figure out why. They give people medicine and other kinds of treatment. They also give advice about diet, exercise, and sleep.\n\n                Doctors use science to figure out what is making people sick. Doctors examine people, listen to them describe their health problems, and do tests to see what is wrong.\n                \n                There are many kinds of doctors. Family and general practitioners are often the first doctors that people go to when they get sick. These doctors treat common problems. They also send patients to other doctors, called specialists. Specialists are experts in different types of health problems.\n                \n                For example, internists focus on problems with internal organs. Pediatricians care for children and babies. Surgeons perform operations, like fixing broken bones or transplanting organs.</p>\n        </div>\n        <div class=\"col-md-4\">\n            <img src=\"./assets/doctors.jpg\" width=\"100%\" height=\"300px;\">\n        </div>\n    </div>\n    \n    \n</div>\n<div class=\"row px-5 py-5\" style=\"background-image: url('./assets/f.jpg')\">\n    <div class=\"col-md-6\">\n        <div class=\"container\">\n            <h1 class=\"text-info\">Diverse people.<br>\n            One purpose.</h1>\n            <p class=\"text-secondary\">We are dreamers, thinkers and do-ers rolled into one.Together, we want to improve the healthcare experience for all humanity. We are guided by our values and driven by our motto to do great. These are not just principles for our products or our company, but they are a reflection of who we are as people.</p>\n        </div>\n    </div>\n    <div class=\"col-md-6\">\n\n    </div>\n</div>\n "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/doctor/doctor-dashboard/doctor-dashboard.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/doctor/doctor-dashboard/doctor-dashboard.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n  <div>\n        <!-- A grey horizontal navbar that becomes vertical on small screens -->\n      <nav class=\"navbar navbar-expand-sm bg-dark\">\n          \n        <!-- Links -->\n        <ul class=\"navbar-nav ml-auto\">\n          <li class=\"nav-item\">\n            <a routerLink=\"/doctordashboard/profile\" class=\"nav-link text-white\">Profile</a>\n          </li>\n          <li class=\"nav-item\">\n            <a routerLink=\"/doctordashboard/viewrequests\" class=\"nav-link text-white\">View Requests</a>\n          </li>\n          <li class=\"nav-item\">\n              <a routerLink=\"/doctordashboard/myappointments\" class=\"nav-link text-white\">My Appointments</a>\n            </li>\n          <li class=\"nav-item\">\n              <a routerLink=\"/doctordashboard/paymentstatus\" class=\"nav-link text-white\">Payment Status</a>\n            </li>\n          <li class=\"nav-item\">\n                <button type=\"submit\" class=\"nav-link btn btn-info mr-3\" (click)=\"logout()\">Logout</button>\n          </li>\n          \n          \n          \n          \n        </ul>\n      </nav>\n      <router-outlet></router-outlet>\n        </div>\n      \n    \n    "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/doctor/doctor-dashboard/doctorprofile/doctorprofile.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/doctor/doctor-dashboard/doctorprofile/doctorprofile.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-3\">\n                    \n            </div>\n            <div class=\"col-md-6 mt-5\">\n                    <div *ngIf=\"b\">\n                        <div *ngFor=\"let currentUser of currentUser\">\n                                <h3><marquee>Welcome {{currentUser.duname}} </marquee></h3>\n                        <h2 class=\"text-center  text-info\">View Profile</h2>\n                    <table class=\"table  h6 \">\n                            <tr>\n                                    <td><b>UserName</b></td>\n                                    <td>{{currentUser.name}}</td>\n                                </tr>\n                        <tr>\n                            <td><b>Name</b></td>\n                            <td>{{currentUser.duname}}</td>\n                        </tr>\n                        <tr>\n                            <td><b>DOB</b></td>\n                            <td>{{currentUser.date}}</td>\n                        </tr>\n                        <tr>\n                            <td><b>Mail Id</b></td>\n                            <td>{{currentUser.email}}</td>\n                        </tr>\n                        <tr>\n                            <td><b>Phone</b></td>\n                            <td>{{currentUser.mobilenumber}}</td>\n                        </tr>\n                        <tr>\n                            <td><p>Area</p></td>\n                            <td>{{currentUser.area}}</td>\n                        </tr>\n                        <tr>\n                            <td><b>Specialization</b></td>\n                            <td>{{currentUser.specialization}}</td>\n                        </tr>\n                        <tr>\n                            <td><b>Experience</b></td>\n                            <td>{{currentUser.experience}}</td>\n                        </tr>\n                        <tr>\n                            <td><b>Timings</b></td>\n                            <td>{{currentUser.timings}}</td>\n                        </tr>\n                        <tr>\n                            <td><b>About</b></td>\n                            <td>{{currentUser.about}}</td>\n                        </tr>\n    \n                    </table>\n                    <button type=\"submit\" class=\"btn btn-success d-block mx-auto\" (click)=\"edit(currentUser)\">Edit</button>\n                    </div>\n                </div>\n    \n    \n                    <div *ngIf=\"!b\">\n                            <h2 class=\"text-center  text-info\">Edit Profile</h2>\n                            \n                        <form #ref=\"ngForm\" (ngSubmit)=\"submitEditData(ref.value)\">\n                                <table class=\"table  h6\">\n                                        <tr>\n                                                <td><b>UserName</b></td>\n                                                <td><input type=\"text\" name=\"name\" id=\"\" value=\"objectToUpdate.name\" class=\"form-control\" [(ngModel)]=objectToUpdate.name readonly></td>\n                                            </tr>\n                                        <tr>\n                                            <td><b>Name</b></td>\n                                            <td><input type=\"text\" name=\"duname\" id=\"\" value=\"objectToUpdate.duname\" class=\"form-control\" [(ngModel)]=objectToUpdate.duname></td>\n                                        </tr>\n                                        <tr>\n                                            <td><b>DOB</b></td>\n                                            <td><input type=\"date\" name=\"date\" value=\"objectToUpdate.date\" class=\"form-control\" [(ngModel)]=objectToUpdate.date></td>\n                                        </tr>\n                                        <tr>\n                                            <td><b>Email</b></td>\n                                            <td><input type=\"text\" name=\"email\" value=\"objectToUpdate.email\" class=\"form-control\" [(ngModel)]=objectToUpdate.email></td>\n                                        </tr>\n                                        <tr>\n                                            <td><b>Phone</b></td>\n                                            <td><input type=\"number\" name=\"mobileno\" value=\"objectToUpdate.mobilenumber\" class=\"form-control\" [(ngModel)]=objectToUpdate.mobilenumber></td>\n                                        </tr>\n                                        <tr>\n                                            <td><b>Area</b></td>\n                                            <td><input type=\"text\" name=\"area\" value=\"objectToUpdate.area\" class=\"form-control\" [(ngModel)]=objectToUpdate.area></td>\n                                        </tr>\n                                        <tr>\n                                            <td><b>specialization</b></td>\n                                            <td><input type=\"text\" name=\"specialization\" value=\"objectToUpdate.specialization\" class=\"form-control\" [(ngModel)]=objectToUpdate.specialization></td>\n                                        </tr>\n                                        <tr>\n                                            <td><b>Experience</b></td>\n                                            <td><input type=\"text\" name=\"experience\" value=\"objectToUpdate.experience\" class=\"form-control\" [(ngModel)]=objectToUpdate.experience></td>\n                                        </tr>\n                                        <tr>\n                                            <td><b>Timings</b></td>\n                                            <td><input type=\"text\" name=\"timings\" value=\"objectToUpdate.timings\" class=\"form-control\" [(ngModel)]=objectToUpdate.timings></td>\n                                        </tr>\n                                        <tr>\n                                            <td><b>About</b></td>\n                                            <td><input type=\"text\" name=\"about\" value=\"objectToUpdate.about\" class=\"form-control\" [(ngModel)]=objectToUpdate.about></td>\n                                        </tr>\n                                        <tr>\n                                            <td><button type=\"submit\" class=\"btn btn-info\">Save</button></td>\n                                        </tr>\n                                    </table>\n                        </form>\n                        </div>\n            </div>\n            <div class=\"col-md-3\"></div>\n        </div>\n    </div>\n    "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/doctor/doctor-dashboard/myappointments/myappointments.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/doctor/doctor-dashboard/myappointments/myappointments.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\">\n\n        <div class=\"table-responsive\">\n        <table class=\"table table-bordered\">\n            <thead class=\"bg-info text-white\">\n                <th>Patient Name</th>\n                <th>Appointment Date</th>\n                <th>Appointment Time</th>\n                <th>Email</th>\n                \n                <th>Mobile Number</th>\n                <th>Area</th>\n                \n            </thead>\n        \n        <tr *ngFor=\"let obj of bookings\">\n                       \n                    <td>{{obj.patientname}}</td>\n                    <td>{{obj.appdate}}</td>\n                    <td>{{obj.apptime}}</td>\n                    <td>{{obj.patientemail}}</td>\n                    <td>{{obj.patientnumber}}</td>\n                    <td>{{obj.patientarea}}</td>\n    \n                    \n            </tr>\n        \n        </table>\n        </div>\n        </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/doctor/doctor-dashboard/paymentstatus/paymentstatus.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/doctor/doctor-dashboard/paymentstatus/paymentstatus.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\">\n   \n<table class=\"table table-bordered\">\n    <thead class=\"bg-info text-white\">\n        <th>patient name</th>\n        <th>account number</th>\n        <th>address</th>\n        <th>amount</th>\n        <th>date</th>\n        <th>ifsccode</th> \n        <th>Payment Status</th>\n        \n    </thead>\n    <tr *ngFor=\"let pay of paymentData\">\n        <td>{{pay.patientname}}</td>\n        <td>{{pay.accountnumber}}</td>\n        <td>{{pay.address}}</td>\n        <td>{{pay.amount}}</td>\n        <td>{{pay.date}}</td>\n        <td>{{pay.ifsccode}}</td>\n        <td>{{pay.paystatus}}</td>\n\n    </tr>\n</table>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/doctor/doctor-dashboard/viewrequests/viewrequests.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/doctor/doctor-dashboard/viewrequests/viewrequests.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\">\n    <table class=\"table table-bordered\">\n        <thead class=\"bg-info text-white\">\n            <th>patient name</th>\n            <th>Email Id</th>\n            <th>Mobile No</th>\n            <th>Area</th>\n            <th>Appointment Date</th>\n            <th>Appointment Time</th>\n            <th>Status</th>\n        </thead>\n        <tr *ngFor=\"let obj of patients\">\n            <td>{{obj.patientname}}</td>\n            <td>{{obj.patientemail}}</td>\n            <td>{{obj.patientnumber}}</td>\n            <td>{{obj.patientarea}}</td>\n            <td><input type=\"date\" name=\"appdate\" class=\"form-control\" #ref></td>\n            <td><input type=\"time\" name=\"apptime\" class=\"form-control\" #ref1></td>\n            \n            <td>\n                <div *ngIf=\"obj != undefined \">\n                    <button type=\"submit\" class=\"btn btn-primary\" (click)=accept(obj,ref.value,ref1.value)>accept</button>\n                    <button type=\"submit\" class=\"btn btn-danger ml-1\" (click)=reject(obj)>reject</button>\n                </div>\n            </td>\n        </tr>\n    </table>\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/doctors/doctors.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/doctors/doctors.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\">\n\n        <div class=\"table-responsive\">\n                <div class=\"row\">\n                    <div class=\"col-md-3\"></div>\n                    <div class=\"col-md-6\">\n                         <input class=\"float-right mb-3\" type=\"text\" name=\"search\" class=\"text-center form-control mb-3\" id=\"\" placeholder=\"search by Name/ Area/ Specialization\" [(ngModel)]='searchTerm'>\n                    </div>\n                    <div class=\"col-md-3\"></div>\n                </div>\n        <table class=\"table\">\n                \n            <thead class=\"bg-info text-white\">\n                <th>Doctor Name</th>\n                <th>Email</th>\n                <th>Exp</th>\n                <th>Spec</th>\n                <th>Area</th>\n                \n            </thead>\n        \n        <tr *ngFor=\"let obj of list | search : searchTerm\">\n                       \n                    <td>{{obj.name}}</td>\n                    <td>{{obj.email}}</td>\n                    <td>{{obj.experience}}</td>\n                    <td>{{obj.specilization}}</td>\n                    <td>{{obj.area}}</td>\n  \n        \n                    \n               \n            </tr>\n        \n        </table>\n        </div>\n        </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n  <div id=\"demo\" class=\"carousel slide\" data-ride=\"carousel\">\n  \n          <!-- Indicators -->\n          <ul class=\"carousel-indicators\">\n            <li data-target=\"#demo\" data-slide-to=\"0\" class=\"active\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"1\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"2\"></li>\n          </ul>\n        \n          <!-- The slideshow -->\n          <div class=\"carousel-inner\">\n            <div class=\"carousel-item active\">\n              <img src=\"assets/1.jpg\" alt=\"Los Angeles\" width=\"100%\" height=\"500px\">\n              <div class=\"carousel-caption\">\n                              </div>\n            </div>\n            <div class=\"carousel-item\">\n              <img src=\"assets/2.jpg\" alt=\"Chicago\" width=\"100%\" height=\"500px\">\n            </div>\n            <div class=\"carousel-item\">\n              <img src=\"assets/5.jpg\" alt=\"New York\"width=\"100%\" height=\"500px\">\n            </div>\n            <div class=\"carousel-item\">\n              <img src=\"assets/6.jpg\" alt=\"New York\" style=\"width:100%\" height=\"500px\">\n            </div>\n            <div class=\"carousel-item\">\n              <img src=\"assets/7.jpg\" alt=\"New York\" style=\"width:100%\" height=\"500px\">\n            </div>\n          </div>\n        \n          <!-- Left and right controls -->\n          <a class=\"carousel-control-prev\" href=\"#demo\" data-slide=\"prev\">\n            <span class=\"carousel-control-prev-icon\"></span>\n          </a>\n          <a class=\"carousel-control-next\" href=\"#demo\" data-slide=\"next\">\n            <span class=\"carousel-control-next-icon\"></span>\n          </a>\n        \n        </div>\n        <div class=\"row mt-5\">\n          <div class=\"col-md-8\">\n            <h3 class=\"text-info text-center\">About Us</h3>\n            <p>Whether they are diagnosing an illness or treating an injury, a Medical Doctor is essentially concerned with restoring a patient’s optimum health. Medical Doctors perform health assessments, run diagnostic tests, prescribe medication, create treatment plans and provide health and wellness advice to patients. Medical Doctors can specialize in specific areas of health, such as dermatology, neurology, gastroenterology or gynecology.</p>\n            \n            \n          </div>\n          <div class=\"col-md-4\">\n            <img src=\"assets/doctor1.png\" height=\"80%\" >\n          </div>\n        </div>\n        <h1 class=\"text-info text-center\">Why Choose Us</h1>\n        <h4 class=\"text-center\">We Provide Best Services For Patient\n  \n        </h4>\n        <div class=\"card-deck mt-5\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                  <h3 class=\"text-info text-center\"> MODERN CLINIC</h3>\n                </div>\n                <div class=\"card-body\">\n                    <img src=\"assets/8.png\" class=\"d-block mx-auto\" height=\"60%\" width=\"50%\">\n  \n                </div>\n                <div class=\"card-footer\">\n                     <p>A system in which medical doctors and other healthcare professionals (such as nurses, pharmacists, and therapists) treat symptoms and diseases using drugs, radiation, or surgery.</p>\n                </div>\n            </div>\n            <div class=\"card\">\n                  <div class=\"card-header\">\n                    <h3 class=\"text-info text-center\">\n                          QUALIFIED DOCTORS</h3>\n                  </div>\n                  <div class=\"card-body\">\n                      <img src=\"assets/d.png\" height=\"60%\" width=\"50%\" class=\"d-block mx-auto\">\n    \n                  </div>\n                  <div class=\"card-footer\">\n                       <p>Medical doctors examine, diagnose and treat patients. They can specialize in a number of medical areas, such as pediatrics, anesthesiology or cardiology, or they can work as general practice physicians.</p>\n                  </div>\n              </div>\n              <div class=\"card\">\n                      <div class=\"card-header\">\n                        <h3 class=\"text-info text-center\">\n                              EMERGENCY CASES</h3>\n                      </div>\n                      <div class=\"card-body\">\n                          <img src=\"assets/m.png \" class=\"d-block mx-auto\" height=\"60%\" width=\"50%\">\n        \n                      </div>\n                      <div class=\"card-footer\">\n                           <p>An emergency is a situation that poses an immediate risk to health, life, property, or environment. Most emergencies require urgent intervention to prevent a worsening of the situation, although in some situations,</p>\n                      </div>\n                  </div>\n        </div>\n        <div class=\"card\">\n          <div class=\"card-body my-auto \" style=\"background-image: url('assets/f.jpg');height:400px\" height=\"600px\" width=\"100%\">\n            <div class=\"row\">\n              <div class=\"col-md-8\">\n                  <div id=\"carouselContent\" class=\"carousel slide\" data-ride=\"carousel\">\n                      <div class=\"carousel-inner\" role=\"listbox\">\n                          <div class=\"carousel-item text-center p-4\">\n                              \n                              <h1><p class=\"text-info\">We help you to find the best doctor around you</p></h1>\n                          </div>\n                          <div class=\"carousel-item active text-center p-4\">\n                               <h1><p class=\"text-info\">We Provide Top Medical Services</p></h1>\n                          </div>\n                          <div class=\"carousel-item text-center p-4\">\n                              \n                              <h1><p class=\"text-info\">Growing up your \n                                  children with our most \n                                  smart monitization </p></h1>\n                          </div>\n                      </div>\n                      <a class=\"carousel-control-prev \" href=\"#carouselContent\" role=\"button\" data-slide=\"prev\">\n                          <span class=\"carousel-control-prev-icon \" aria-hidden=\"true\"></span>\n                          <span class=\"sr-only\">Previous</span>\n                      </a>\n                      <a class=\"carousel-control-next\" href=\"#carouselContent\" role=\"button\" data-slide=\"next\">\n                          <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\n                          <span class=\"sr-only\">Next</span>\n                      </a>\n                  </div>\n              </div>\n              <div class=\"col-md-4\"></div>\n            </div>\n            </div>\n            </div>\n            <div class=\"card\">\n            <!--<div style=\"height:50px\"class=\"bg-dark text-white text-center\">© Copyright ©2019 All rights reserved.</div>-->\n           <div class=\"card-footer bg-dark text-white text-center h5\">\n            © Copyright ©2019 All rights reserved.\n           </div>\n        \n          </div>\n\n        "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/login.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-4\"></div>\n    <div class=\"col-sm-4 my-3 \">\n        <div>\n            <div class=\"container py-3\" >\n                <h3 class=\" text-center text-dark mb-2\" style=\"text-decoration:underline\">Login Form</h3>\n                    <form class=\"form\" style=\"background-color: rgba(191, 213, 231, 0.575);border:5px solid rgb(53, 153, 235);border-radius: 15px\" #ref=ngForm (ngSubmit)=\"submit(ref.value)\">\n                        \n                        <div class=\"text-center\"> \n                        \n                        \n                            <div class=\"form-check\">   \n                                <label for=\"loginusertype\" class=\"form-check-label\">\n                                <input type=\"radio\" name=\"usertype\" id=\"loginusertype\" value=\"patient\" class=\"form-check-input\" ngModel>\n                                Patient</label>\n                                \n                            </div>\n                            <div class=\"form-check\">   \n                                    <label for=\"loginusertype1\" class=\"form-check-label\">\n                                    <input type=\"radio\" name=\"usertype\" id=\"loginusertype1\" value=\"doctor\" class=\"form-check-input\" ngModel>\n                                    Doctor</label>\n                                    \n                                    \n                                </div>\n                            </div>\n                                        \n                        <div class=\"form-group text-info ml-2 mr-2\">\n                            <label for=\"loginname\" ><h5>UserName</h5></label>\n                            <input type=\"text\" name=\"name\" class=\"form-control\" ngModel id=\"loginname\" placeholder=\"enter username\">\n                        </div>\n                        <div class=\"form-group text-info ml-2 mr-2\">\n                            <label for=\"loginpassword\"><h5>Password</h5></label>\n                            <input type=\"password\" name=\"password\" class=\"form-control\" ngModel id=\"loginpassword\" placeholder=\"enter your password\">\n                        </div>\n                        \n                                \n                        <div class=\"form-group text-center\">\n                            <button type=\"submit\" class=\"btn btn-success\">Login</button>\n                        </div>   \n                        <div class=\"text-center mb-4\">\n                          <b> <span class=\"text-info \" >ForgotPassword?</span></b>\n                            <a routerLink=\"/nav/forgotpassword\" class=\"text-success\">Click Here</a>\n                            </div>     \n                        <!--<h6 class=\"text-center text-danger\" *ngIf=\"loginDetails\">{{loginDetails}}</h6>--> \n                       \n                    </form>\n                   \n                </div>\n        \n    </div>\n    <div class=\"col-sm-4\"></div>\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/nav/changepassword/changepassword.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/nav/changepassword/changepassword.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body >\n    <div class=\"container-fluid\">\n    <div class=\"row\">\n        <div class=\"col-sm-4\"></div>\n        <div class=\"col-sm-4 my-3 \">\n                <div class=\"container bg-info\" style=\"border:2px solid black\">\n                    <h3 class=\" text-center\">Login Form</h3>\n                        <form class=\"form \" #ref=ngForm (ngSubmit)=\"changepassword(ref.value)\">\n                            \n                                    <div class=\"form-check\">   \n                                            <label for=\"loginusertype\" class=\"form-check-label\">\n                                            <input type=\"radio\" name=\"usertype\" id=\"loginusertype\" value=\"patient\" class=\"form-check-input\" ngModel>\n                                            Patient</label>\n                                            \n                                        </div>\n                                        <div class=\"form-check\">   \n                                                <label for=\"loginusertype1\" class=\"form-check-label\">\n                                                <input type=\"radio\" name=\"usertype\" id=\"loginusertype1\" value=\"doctor\" class=\"form-check-input\" ngModel>\n                                                Doctor</label>\n                                                \n                                            </div>\n                                            <br>\n                            <div class=\"form-group\">\n                                <label for=\"loginname\" ><h5>UserName</h5></label>\n                                <input type=\"text\" name=\"name\" class=\"form-control\" ngModel id=\"loginname\" placeholder=\"enter your login name\">\n                            </div>\n                            <div class=\"form-group\">\n                                <label for=\"loginpassword\"><h5>Enter new Password</h5></label>\n                                <input type=\"password\" name=\"password\" class=\"form-control\" ngModel id=\"loginpassword\" placeholder=\"enter your password\">\n                            </div>\n                            \n                                    \n                            <div class=\"form-group text-center\">\n                                <button type=\"submit\" class=\"btn btn-secondary\">Submit</button>\n                            </div>        \n                        </form>             \n                    </div>\n        </div>\n        <div class=\"col-sm-4\"></div>\n     </div>\n    </div>\n    </body>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/nav/forgotpassword/forgotpassword.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/nav/forgotpassword/forgotpassword.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-3\"></div>\n    <div class=\"col-sm-6 my-3 \">\n            <div class=\"container bg-info py-3\" >\n                    <form class=\"form\" #ref=ngForm (ngSubmit)=\"password(ref.value)\">\n                        <div class=\"form-group\">\n                            <label for=\"loginname\" ><h5>UserName</h5></label>\n                            <input type=\"text\" name=\"name\" class=\"form-control\" ngModel id=\"loginname\" placeholder=\"enter your login name\">\n                        </div>\n                        <br/>\n                        <div class=\"form-check\">   \n                            <label for=\"loginusertype\" class=\"form-check-label\">\n                            <input type=\"radio\" name=\"usertype\" id=\"loginusertype\" value=\"patient\" class=\"form-check-input\" ngModel>\n                            Patient</label>\n                            \n                        </div>\n                        <div class=\"form-check\">   \n                                <label for=\"loginusertype1\" class=\"form-check-label\">\n                                <input type=\"radio\" name=\"usertype\" id=\"loginusertype1\" value=\"doctor\" class=\"form-check-input\" ngModel>\n                                Doctor</label>\n                        </div>\n                        <div class=\"form-group text-center\">\n                                <button type=\"submit\" class=\"btn btn-dark text-white\">Login</button>\n                            </div>\n                        </form>\n                        </div>\n                        </div>\n                        <div class=\"col-sm-3\"></div>\n                        </div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/nav/nav.component.html":
/*!******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/nav/nav.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n    <!-- A grey horizontal navbar that becomes vertical on small screens -->\n  <nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">\n      <a class=\"navbar-brand\" routerLink=\"home\">\n          <img src=\"assets/logo.png\" alt=\"Logo\" style=\"width:60px;height: 40px;\"><label class=\"text-info pl-2 h3 hide\">Online Doctor Appointment</label>\n      </a>\n      <button type=\"button\" class=\"navbar-toggler\" data-toggle=\"collapse\" data-target=\"#a\"><!--create navbar toggler button-->\n        <span class=\"navbar-toggler-icon\"></span>                                 <!--create navbar button-->\n     </button>\n      <div class=\"collapse navbar-collapse\" id=\"a\">\n    <!-- Links -->\n    <ul class=\"navbar-nav ml-auto\">\n      <li class=\"nav-item\">\n        <a routerLink=\"home\" class=\"nav-link text-white\">Home</a>\n      </li>\n      <li class=\"nav-item\">\n        <a routerLink=\"doctors\" class=\"nav-link text-white\">Doctors</a>\n      </li>\n      <li class=\"nav-item\">\n        <a routerLink=\"aboutus\" class=\"nav-link text-white\">About Us</a>\n      </li>\n     \n      <!--<li class=\"nav-item\">\n        <a routerLink=\"patientsdashboard\" class=\"nav-link text-white\">Patients Dashboard</a>\n      </li>-->\n      <!--<li class=\"nav-item\">\n          <a routerLink=\"doctorsdashboard\" class=\"nav-link text-white\">Doctors Dashboard</a>\n      </li>-->\n      \n      <!--<li class=\"nav-item\">\n        <a routerLink=\"login\" class=\"nav-link btn btn-info mr-3\" *ngIf=ls.isLoggedin >\n          <i class=\"fa fa-lock\" style=\"margin-right: 10px;\"></i>Logout\n        </a>\n      </li>-->\n      <li class=\"nav-item\">\n        <a routerLink=\"register\" class=\"nav-link btn btn-info\">\n          <i class=\"fa fa-sign-in\" style=\"margin-right: 10px;\"></i>Register\n        </a>\n      </li>\n      <li class=\"nav-item\">\n          <a routerLink=\"login\" class=\"nav-link btn btn-info ml-3\">\n            <i class=\"fa fa-lock\" style=\"margin-right: 10px;\"></i>Login\n          </a>\n        </li>\n      \n      \n    </ul>\n  </div>\n  </nav>\n  <router-outlet></router-outlet>\n    </div>\n  \n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/nav/otp/otp.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/nav/otp/otp.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h2 class=\"text-center text-warning\">OTP Verification</h2>\n    <div class=\"row mt-3\">\n        <div class=\"col-md-3\"></div>\n        <div class=\"col-md-6\">\n            <form #ref=\"ngForm\" (ngSubmit)=\"otp(ref.value)\">\n              <div class=\"form-group\">\n                  <label for=\"\">Enter OTP</label>\n                  <input type=\"number\" name=\"OTP\" class=\"form-control\" ngModel>\n              </div>\n              <div class=\"form-group\">\n                <button class=\"btn btn-success\">Submit</button>\n              </div>    \n            </form>\n        </div>\n        <div class=\"col-md-3\"></div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/makepayment/makepayment.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/patient/patient-dashboard/makepayment/makepayment.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-3\"></div>\n    <div class=\"col-sm-6 px-3 py-3 mt-3 text-info \">\n            <div class=\"container\">\n                    <form class=\"form\" style=\"background-color: rgba(191, 213, 231, 0.575);border:5px solid rgb(53, 153, 235);\" #pay=ngForm (ngSubmit)=payment(pay.value)>\n                        <h1 class=\"text-center\">Make your payment</h1>\n                            <div class=\"form-group\">\n                                    <label for=\"doctor name\"><span>*</span>Doctor Username</label>\n                                    <input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\" #reguser=\"ngModel\" ngModel required>\n                                    <div *ngIf=\"reguser.invalid&&(reguser.dirty||reguser.touched)\">\n                                                <div *ngIf=\"reguser.errors.required\">\n                                                <p class=\"text-danger\">*Doctorname is manidatory</p>\n                                              </div>\n                                              </div>\n                            </div>\n                            <div class=\"form-group\">\n                                    <label for=\"address\">address</label>\n                                    <input type=\"text\" name=\"address\" id=\"address\" class=\"form-control\" ngModel>\n                            </div>\n                            <div class=\"form-group\">\n                                    <label for=\"amount\"><span>*</span>amount</label>\n                                    <input type=\"number\" name=\"amount\" id=\"amount\" class=\"form-control\" ngModel #amount=\"ngModel\" required>\n                                    <div *ngIf=\"amount.invalid&&(amount.dirty||amount.touched)\">\n                                                <div *ngIf=\"amount.errors.required\">\n                                                <p class=\"text-danger\">*Amount is manidatory</p>\n                                              </div>\n                                              </div>\n                            </div>\n                            <div class=\"form-group\">\n                                    <label for=\"date\">date</label>\n                                    <input type=\"date\" name=\"date\" id=\"date\" class=\"form-control\" ngModel>\n                            </div>\n                            <div class=\"form-group\">\n                                    <label for=\"code\"><span>*</span>IFSC code</label>\n                                    <input type=\"text\" name=\"ifsccode\" id=\"code\" class=\"form-control\" ngModel #ifsc=\"ngModel\" ngModel required>\n                                    <div *ngIf=\"ifsc.invalid&&(ifsc.dirty||ifsc.touched)\">\n                                                <div *ngIf=\"ifsc.errors.required\">\n                                                <p class=\"text-danger\">*IFSC is manidatory</p>\n                                              </div>\n                                              </div>\n                            </div>\n                            <div class=\"form-group\">\n                                    <label for=\"account\"><span>*</span>account number</label>\n                                    <input type=\"text\" name=\"accountnumber\" id=\"account\" class=\"form-control\" ngModel #account=\"ngModel\" required>\n                                    <div *ngIf=\"account.invalid&&(account.dirty||account.touched)\">\n                                                <div *ngIf=\"account.errors.required\">\n                                                <p class=\"text-danger\">*Account Number is manidatory</p>\n                                              </div>\n                                              </div>\n                            </div>\n                            <div class=\"form-group text-center\">\n                                    <button type=\"submit\" class=\"btn btn-primary text-center\">pay</button>\n                            </div>\n                \n                    </form>\n                    </div>\n    </div>\n    <div class=\"col-sm-3\"></div>\n    </div>\n    "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/mybookings/mybookings.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/patient/patient-dashboard/mybookings/mybookings.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\">\n\n        <div class=\"table-responsive\">\n        <table class=\"table\">\n            <thead class=\"bg-info text-white\">\n                <th>Doctor Name</th>\n                <th>Appointment Date</th>\n                <th>Appointment Time</th>\n                <th>Email</th>\n                <th>Exp</th>\n                <th>Spec</th>\n                <th>Area</th>\n                \n            </thead>\n        \n        <tr *ngFor=\"let obj of bookings\">\n                       \n                    <td>{{obj.doctorname}}</td>\n                    <td>{{obj.appdate}}</td>\n                    <td>{{obj.apptime}}</td>\n                    <td>{{obj.doctoremail}}</td>\n                    <td>{{obj.doctorexp}}</td>\n                    <td>{{obj.doctorspec}}</td>\n                    <td>{{obj.doctorarea}}</td>\n    \n                    \n            </tr>\n        \n        </table>\n        </div>\n        </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/patient-dashboard.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/patient/patient-dashboard/patient-dashboard.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<div>\n        <!-- A grey horizontal navbar that becomes vertical on small screens -->\n      <nav class=\"navbar navbar-expand-sm bg-dark\">\n          \n        <!-- Links -->\n        <ul class=\"navbar-nav ml-auto\">\n          <li class=\"nav-item\">\n            <a routerLink=\"/patientdashboard/profile\" class=\"nav-link text-white\">Profile</a>\n          </li>\n          <li class=\"nav-item\">\n            <a routerLink=\"/patientdashboard/viewdoctors\" class=\"nav-link text-white\">View Doctors</a>\n          </li>\n          <li class=\"nav-item\">\n            <a routerLink=\"/patientdashboard/mybookings/\" class=\"nav-link text-white\">My Bookings</a>\n          </li>\n          <li class=\"nav-item\">\n            <a routerLink=\"/patientdashboard/makepayment\" class=\"nav-link text-white\">Make Payments</a>\n          </li>\n          <li class=\"nav-item\">\n            <a routerLink=\"/patientdashboard/paymenthistory\" class=\"nav-link text-white\">Payment History</a>\n          </li>\n          <li class=\"nav-item\">\n                <button type=\"submit\" class=\"nav-link btn btn-info mr-3\" (click)=\"logout()\">Logout</button>\n          </li>\n          \n          \n          \n          \n        </ul>\n      </nav>\n      <router-outlet></router-outlet>\n        </div>\n      \n    \n    "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/patientprofile/patientprofile.component.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/patient/patient-dashboard/patientprofile/patientprofile.component.html ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-3\">\n                \n        </div>\n        <div class=\"col-md-6 mt-5\">\n                <div *ngIf=\"b\">\n                    <div *ngFor=\"let currentUser of currentUser\">\n                        <h3><marquee>Welcome {{currentUser.puname}} </marquee></h3>\n                    <h2 class=\"text-center text-info\">View Profile</h2>\n                <table class=\"table h6 \">\n                    <tr>\n                        <td><b>Name</b></td>\n                        <td>{{currentUser.puname}}</td>\n                    </tr>\n                    <tr>\n                        <td><b>UserName</b></td>\n                        <td>{{currentUser.name}}</td>\n                    </tr>\n                    <tr>\n                        <td><b>DOB</b></td>\n                        <td>{{currentUser.date}}</td>\n                    </tr>\n                    <tr>\n                        <td><b>Mail Id</b></td>\n                        <td>{{currentUser.email}}</td>\n                    </tr>\n                    <tr>\n                        <td><b>mobileNumber</b></td>\n                        <td>{{currentUser.mobilenumber}}</td>\n                    </tr>\n                    <tr>\n                        <td><b>Area</b></td>\n                        <td>{{currentUser.area}}</td>\n                    </tr>\n\n                </table>\n                <button type=\"submit\" class=\"btn btn-success d-block mx-auto\" (click)=\"edit(currentUser)\">Edit</button>\n                </div>\n            </div>\n\n\n                <div *ngIf=\"!b\">\n                        <h2 class=\"text-center text-info\">Edit Profile</h2>\n                        \n                    <form #ref=\"ngForm\" (ngSubmit)=\"submitEditData(ref.value)\">\n                            <table class=\"table h6\">\n                                    <tr>\n                                            <td><b>UserName</b></td>\n                                            <td><input type=\"text\" name=\"name\" id=\"\" value=\"objectToUpdate.name\" class=\"form-control\" [(ngModel)]=objectToUpdate.name readonly></td>\n                                        </tr>\n\n                                    <tr>\n                                        <td><b>Name</b></td>\n                                        <td><input type=\"text\" name=\"puname\" id=\"\" value=\"objectToUpdate.puname\" class=\"form-control\" [(ngModel)]=objectToUpdate.puname></td>\n                                    </tr>\n                                    <tr>\n                                        <td><b>DOB</b></td>\n                                        <td><input type=\"date\" name=\"date\" value=\"objectToUpdate.date\" class=\"form-control\" [(ngModel)]=objectToUpdate.date></td>\n                                    </tr>\n                                    <tr>\n                                        <td><b>Email-Id</b></td>\n                                        <td><input type=\"text\" name=\"email\" value=\"objectToUpdate.email\" class=\"form-control\" [(ngModel)]=objectToUpdate.email></td>\n                                    </tr>\n                                    <tr>\n                                        <td><b>Phone</b></td>\n                                        <td><input type=\"number\" name=\"mobileno\" value=\"objectToUpdate.mobilenumber\" class=\"form-control\" [(ngModel)]=objectToUpdate.mobilenumber></td>\n                                    </tr>\n                                    <tr>\n                                        <td><b>Area</b></td>\n                                        <td><input type=\"text\" name=\"area\" value=\"objectToUpdate.area\" class=\"form-control\" [(ngModel)]=objectToUpdate.area></td>\n                                    </tr>\n                                    <tr>\n                                        <td><button type=\"submit\" class=\"btn btn-info\">Save</button></td>\n                                    </tr>\n                                </table>\n                    </form>\n                    </div>\n        </div>\n        <div class=\"col-md-3\"></div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/paymenthistory/paymenthistory.component.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/patient/patient-dashboard/paymenthistory/paymenthistory.component.html ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h1 class=\"text-center\">payments history</h1>\n<table class=\"table table-bordered\">\n    <thead class=\"bg-info text-white\">\n        <th>accountnumber</th>\n        <th>address</th>\n        <th>amount</th>\n        <th>date</th>\n        <th>ifsccode</th> \n        <th>doctorname</th>\n    </thead>\n    <tr *ngFor=\"let pay of paymentData\">\n        <td>{{pay.accountnumber}}</td>\n        <td>{{pay.address}}</td>\n        <td>{{pay.amount}}</td>\n        <td>{{pay.date}}</td>\n        <td>{{pay.ifsccode}}</td>\n        <td>{{pay.name}}</td>\n\n    </tr>\n</table>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/viewdoctors/viewdoctors.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/patient/patient-dashboard/viewdoctors/viewdoctors.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\">\n\n    <div class=\"table-responsive\">\n            <div class=\"row\">\n                <div class=\"col-md-3\"></div>\n                <div class=\"col-md-6\">\n                        <input class=\"float-right mb-3\" type=\"text\" name=\"searchWord\" class=\"text-center form-control mb-3\" id=\"\" placeholder=\"search by Name/ Area/ Specialization\" [(ngModel)]='searchWord'>\n                </div>\n                <div class=\"col-md-3\"></div>\n            </div>\n    <table class=\"table\">\n            \n        <thead class=\"bg-info text-white\">\n            <th>Doctor Name</th>\n            <th>Email</th>\n            <th>Exp</th>\n            <th>Spec</th>\n            <th>Area</th>\n            <th>Timings</th>\n            \n            <th>status</th>\n        </thead>\n    \n    <tr *ngFor=\"let obj of list | search : searchWord\">\n                   \n                <td>{{obj.name}}</td>\n                <td>{{obj.email}}</td>\n                <td>{{obj.experience}}</td>\n                <td>{{obj.specilization}}</td>\n                <td>{{obj.area}}</td>\n                <td>{{obj.timing}}</td>  \n    \n                \n            <td>\n                <button class=\"btn btn-success\" type=\"submit\" *ngIf=\"!(currentUser[0].name==obj.patientname)\" (click)=\"changeStatus(obj)\">{{b?\"bookappointment\":\"request pending\"}}</button>\n                <!--<button class=\"btn btn-primary\" *ngIf=\"check \" (click)=\"requestStatus(x)\">check request </button>-->\n                <p *ngIf='currentUser[0].name==obj.patientname'>{{obj.reqstatus}}</p>\n            </td>\n        </tr>\n    \n    </table>\n    </div>\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/registration/doctorregistration/doctorregistration.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/registration/doctorregistration/doctorregistration.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-3\"></div>\n    <div class=\"col-md-6\">\n\n    <form class=\"mt-5\" style=\"border:5px solid rgb(53, 153, 235);background-color: rgba(191, 213, 231, 0.575)\"  #ref=\"ngForm\" (ngSubmit)=\"doctorReg(ref.value)\">\n            <h3 class=\"text-center text-info\"> Doctor Registration Form</h3>\n\n  <table class=\"table\">\n    <tr class=\"form-group\">\n      <td><label><span>*</span>UserName:</label></td>\n      <td><input type=\"text\" name=\"name\" class=\"form-control\" #reguser=\"ngModel\" ngModel minlength=\"5\" required>\n       <div *ngIf=\"reguser.invalid&&(reguser.dirty||reguser.touched)\">\n         <div *ngIf=\"reguser.errors.required\">\n         <p class=\"text-danger\">*username is manidatory</p>\n       </div>\n       <div *ngIf=\"reguser.errors.minlength\">\n         <p class=\"text-danger\">*UserName must be atleast 5 characters long</p>\n       </div>\n     </div>\n     </td>\n    </tr>\n    <tr class=\"form-group\">\n        <td><label><span>*</span>Name:</label></td>\n        <td><input type=\"text\" name=\"duname\" class=\"form-control\" #regusers=\"ngModel\" ngModel required>\n         <div *ngIf=\"regusers.invalid&&(regusers.dirty||regusers.touched)\">\n           <p class=\"text-danger\">*name is manidatory</p>\n         </div>\n         \n       </td>\n      </tr>\n    \n <tr class=\"form-group\">\n    <td><label>Email:</label></td>\n    <td><input type=\"email\" name=\"email\" class=\"form-control\" ngModel></td>\n    </tr>\n    <tr class=\"form-group\">\n        <td><label><span>*</span>Password:</label></td>\n        <td><input type=\"password\" name=\"password\" class=\"form-control\" ngModel  #pw=\"ngModel\" required minlength=\"8\">\n          <div *ngIf=\"pw.invalid && (pw.dirty||pw.touched)\">\n              <div *ngIf=\"pw.errors.required\">\n                <p class=\"text-danger\">*Password is required</p>\n              </div>\n              <div *ngIf=\"pw.errors.minlength\">\n                <p class=\"text-danger\">*Password must be atleast 6 characters long</p>\n              </div>\n            </div>\n        </td>\n      </tr>\n <tr class=\"form-group\">\n <td><label>DateOfBirth:</label></td>\n <td><input type=\"date\" name=\"date\" class=\"form-control\" ngModel></td>\n </tr>\n \n    <tr class=\"form-group\">\n        <td><label><span>*</span>mobile number:</label></td>\n        <td><input type=\"tel\" name=\"mobilenumber\" class=\"form-control\" ngModel></td>\n        </tr>\n        <tr>\n          <td><label>Gender:</label></td>\n          <td><div class=\"form-check\" *ngFor=\"let gender of genders\" >\n            <label for={{gender}} class=\"form-check-label\">\n            <input type=\"radio\" name=\"gender\" id={{gender}} [value]=gender  class=\"form-check-input\" ngModel>{{gender}}\n         </label></div>\n         </td>\n       \n        </tr>\n\n <tr class=\"form-group\">\n    <td><label><span>*</span>Timings:</label></td>\n    <td><input type=\"time\" name=\"timing\" class=\"form-control\" ngModel></td>\n    </tr>\n    <tr class=\"form-group\">\n        <td><label><span>*</span>fees:</label></td>\n        <td><input type=\"number\" name=\"fee\" class=\"form-control\" ngModel required #f=\"ngModel\">\n          <div *ngIf=\"f.invalid && (f.dirty||f.touched)\">\n              <div *ngIf=\"f.errors.required\">\n                <p class=\"text-danger\">*Fee is required</p>\n              </div>\n              </div></td>\n        </tr>\n        \n            <tr class=\"form-group\">\n                <td><label><span>*</span>Experience:</label></td>\n                <td><input type=\"number\" name=\"experience\" class=\"form-control\" ngModel #e=\"ngModel\" required>\n                  <div *ngIf=\"e.invalid && (e.dirty||e.touched)\">\n                      <div *ngIf=\"e.errors.required\">\n                        <p class=\"text-danger\">*Experience is required</p>\n                      </div>\n                      </div></td>\n                </tr>\n                <tr>\n                    <td><label><span>*</span>Area:</label></td>\n                    <td class=\"form-group\">\n                      <select class=\"form-control\" name=\"area\" id=\"\" ngModel #a=\"ngModel\" required>\n                          <option value=\"\" disabled=\"disabled\" selected> Select Area</option>\n                        <option [value]=area *ngFor=\"let area of areas\"  selected>\n                          {{area}}\n                        </option>\n                        \n                  \n                  \n                      </select>\n                      <div *ngIf=\"a.invalid && (a.dirty||a.touched)\">\n                          <div *ngIf=\"a.errors.required\">\n                            <p class=\"text-danger\">*Area is required</p>\n                          </div>\n                          </div>\n                    </td>\n                  </tr>\n                <tr>\n                    <td><label><span>*</span>Specilization:</label></td>\n                    <td class=\"form-group\">\n                      <select class=\"form-control\" name=\"specilization\" id=\"\" ngModel required #s=\"ngModel\">\n                          <option value=\"\" disabled=\"disabled\" selected> Select specilization</option>\n                        <option [value]=specilization *ngFor=\"let specilization of specilizations\"  selected>\n                          {{specilization}}\n                        </option>\n                        \n                  \n                  \n                      </select>\n                      <div *ngIf=\"s.invalid && (s.dirty||s.touched)\">\n                          <div *ngIf=\"s.errors.required\">\n                            <p class=\"text-danger\">*Specilization is required</p>\n                          </div>\n                          </div>\n\n                    </td>\n                  </tr>\n                  <tr>\n                      <td><label>About Doctor:</label></td>\n                      <td><div class=\"form-group\">\n                       <textarea rows=\"5\" class=\"form-control\" name=\"textarea\" ngModel></textarea>\n                      </div></td>\n                    </tr>\n\n\n</table>\n<div class=\"text-center\">\n<button class=\"btn btn-success\" type=\"submit\"> submit</button>\n</div>\n</form>\n</div>\n<div class=\"col-md-3\"></div>\n</div>\n</div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/registration/patientregistration/patientregistration.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/registration/patientregistration/patientregistration.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-3\"></div>\n    <div class=\"col-md-6\">\n\n    <form class=\"mt-5 \" style=\"border:5px solid rgb(53, 153, 235);background-color: rgba(191, 213, 231, 0.575)\"  #ref=\"ngForm\" (ngSubmit)=\"patientReg(ref.value)\">\n            <h3 class=\"text-center text-info\"> Patient Registration Form</h3>\n\n  <table class=\"table\">\n <tr class=\"form-group\">\n   <td><label><span>*</span>UserName:</label></td>\n   <td><input type=\"text\" name=\"name\" class=\"form-control\" #reguser=\"ngModel\" ngModel minlength=\"5\" required>\n    <div *ngIf=\"reguser.invalid&&(reguser.dirty||reguser.touched)\">\n      <div *ngIf=\"reguser.errors.required\">\n      <p class=\"text-danger\">*username is manidatory</p>\n    </div>\n    <div *ngIf=\"reguser.errors.minlength\">\n      <p class=\"text-danger\">*UserName must be atleast 4 characters long</p>\n    </div>\n  </div>\n  </td>\n </tr>\n <tr class=\"form-group\">\n    <td><label>Name:</label></td>\n    <td><input type=\"text\" name=\"puname\" class=\"form-control\" #regusers=\"ngModel\" ngModel required>\n     <div *ngIf=\"regusers.invalid&&(regusers.dirty||regusers.touched)\">\n       <div *ngIf=\"regusers.errors.required\">\n       <p class=\"text-danger\">*name is manidatory</p>\n     </div>\n     \n   </div>\n   </td>\n  </tr>\n <tr class=\"form-group\">\n    <td><label>Email:</label></td>\n    <td><input type=\"email\" name=\"email\" class=\"form-control\" ngModel></td>\n    </tr>\n <tr class=\"form-group\">\n    <td><label><span>*</span>Password:</label></td>\n    <td><input type=\"password\" name=\"password\" class=\"form-control\" ngModel  #pw=\"ngModel\" required minlength=\"6\">\n      <div *ngIf=\"pw.invalid && (pw.dirty||pw.touched)\">\n          <div *ngIf=\"pw.errors.required\">\n            <p class=\"text-danger\">*Password is required</p>\n          </div>\n          <div *ngIf=\"pw.errors.minlength\">\n            <p class=\"text-danger\">*Password must be atleast 6 characters long</p>\n          </div>\n        </div>\n    </td>\n  </tr>\n <tr class=\"form-group\">\n <td><label><span>*</span>MobileNumber:</label></td>\n <td><input type=\"tel\" name=\"mobilenumber\" class=\"form-control\" ngModel #m=\"ngModel\" required>\n  <div *ngIf=\"m.invalid && m.touched\">\n      <div *ngIf=\"m.errors.required\">\n        <p class=\"text-danger\">*Contact number is required</p>\n      </div>\n    </div></td>\n </tr>\n \n    <tr class=\"form-group\">\n        <td><label><span>*</span>DateOfBirth:</label></td>\n        <td><input type=\"date\" name=\"date\" class=\"form-control\" ngModel required #dob=\"ngModel\">\n        <div *ngIf=\"dob.invalid && dob.touched\">\n          <div *ngIf=\"dob.errors.required\">\n            <p class=\"text-danger\">*D.O.B is required</p>\n          </div>\n        </div></td>\n        </tr>\n <tr>\n   <td><label>Gender:</label></td>\n   <td><div class=\"form-check\" *ngFor=\"let gender of genders\" >\n     <label for={{gender}} class=\"form-check-label\">\n     <input type=\"radio\" name=\"gender\" id={{gender}} [value]=gender  class=\"form-check-input\" ngModel>{{gender}}\n  </label></div>\n  \n  </td>\n  \n\n </tr>\n<tr>\n  <td><label>Area:</label></td>\n  <td class=\"form-group\">\n    <select class=\"form-control\" name=\"area\" id=\"\" ngModel>\n        <option value=\"\" disabled=\"disabled\" selected> Select Area</option>\n      <option [value]=area *ngFor=\"let area of areas\"  selected>\n        {{area}}\n      </option>\n      \n\n\n    </select>\n  </td>\n</tr>\n\n\n</table>\n<div class=\"text-center\">\n<button class=\"btn btn-success\" type=\"submit\"> submit</button>\n</div>\n</form>\n</div>\n<div class=\"col-md-3\"></div>\n</div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/registration/registration.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/registration/registration.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\">\n    <div class=\"row\">\n        <div class=\"col-md-3\"></div>\n        <div class=\"col-md-6\">\n            <nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">   \n                    <ul class=\"navbar-nav\">\n                            <li class=\"nav-item\">        \n                                <a routerLink=\"patient\" class=\"nav-link\">Patient Registration</a>\n                            </li>\n                            <li class=\"nav-item\">       \n                                 <a routerLink=\"doctor\" class=\"nav-link\">Doctor Registration</a>\n                            </li>\n                    </ul>\n            </nav>\n         </div>\n         <div class=\"col-md-3\"></div>\n    </div>\n<router-outlet></router-outlet>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/aboutus/aboutus.component.css":
/*!***********************************************!*\
  !*** ./src/app/aboutus/aboutus.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Fib3V0dXMvYWJvdXR1cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/aboutus/aboutus.component.ts":
/*!**********************************************!*\
  !*** ./src/app/aboutus/aboutus.component.ts ***!
  \**********************************************/
/*! exports provided: AboutusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutusComponent", function() { return AboutusComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AboutusComponent = class AboutusComponent {
    constructor() { }
    ngOnInit() {
    }
};
AboutusComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-aboutus',
        template: __webpack_require__(/*! raw-loader!./aboutus.component.html */ "./node_modules/raw-loader/index.js!./src/app/aboutus/aboutus.component.html"),
        styles: [__webpack_require__(/*! ./aboutus.component.css */ "./src/app/aboutus/aboutus.component.css")]
    })
], AboutusComponent);



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _nav_nav_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nav/nav.component */ "./src/app/nav/nav.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./aboutus/aboutus.component */ "./src/app/aboutus/aboutus.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/registration/registration.component.ts");
/* harmony import */ var _registration_patientregistration_patientregistration_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./registration/patientregistration/patientregistration.component */ "./src/app/registration/patientregistration/patientregistration.component.ts");
/* harmony import */ var _registration_doctorregistration_doctorregistration_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./registration/doctorregistration/doctorregistration.component */ "./src/app/registration/doctorregistration/doctorregistration.component.ts");
/* harmony import */ var _doctors_doctors_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./doctors/doctors.component */ "./src/app/doctors/doctors.component.ts");
/* harmony import */ var _nav_otp_otp_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./nav/otp/otp.component */ "./src/app/nav/otp/otp.component.ts");
/* harmony import */ var _nav_changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./nav/changepassword/changepassword.component */ "./src/app/nav/changepassword/changepassword.component.ts");
/* harmony import */ var _nav_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./nav/forgotpassword/forgotpassword.component */ "./src/app/nav/forgotpassword/forgotpassword.component.ts");














const routes = [
    {
        path: '',
        redirectTo: 'nav/home',
        pathMatch: 'full'
    },
    {
        path: 'nav',
        component: _nav_nav_component__WEBPACK_IMPORTED_MODULE_3__["NavComponent"],
        children: [
            {
                path: 'home',
                component: _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"],
            },
            {
                path: 'aboutus',
                component: _aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_5__["AboutusComponent"],
            },
            {
                path: 'doctors',
                component: _doctors_doctors_component__WEBPACK_IMPORTED_MODULE_10__["DoctorsComponent"],
            },
            {
                path: 'login',
                component: _login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"],
            }, {
                path: 'otp',
                component: _nav_otp_otp_component__WEBPACK_IMPORTED_MODULE_11__["OtpComponent"],
            }, {
                path: 'changepassword',
                component: _nav_changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_12__["ChangepasswordComponent"]
            }, {
                path: 'forgotpassword',
                component: _nav_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_13__["ForgotpasswordComponent"]
            },
            {
                path: 'register',
                component: _registration_registration_component__WEBPACK_IMPORTED_MODULE_7__["RegistrationComponent"],
                children: [
                    {
                        path: '',
                        redirectTo: 'patient',
                        pathMatch: 'full'
                    },
                    {
                        path: 'patient',
                        component: _registration_patientregistration_patientregistration_component__WEBPACK_IMPORTED_MODULE_8__["PatientregistrationComponent"],
                    },
                    {
                        path: 'doctor',
                        component: _registration_doctorregistration_doctorregistration_component__WEBPACK_IMPORTED_MODULE_9__["DoctorregistrationComponent"],
                    }
                ]
            }
        ]
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { useHash: true })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'docApp';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _nav_nav_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./nav/nav.component */ "./src/app/nav/nav.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./aboutus/aboutus.component */ "./src/app/aboutus/aboutus.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/registration/registration.component.ts");
/* harmony import */ var _registration_patientregistration_patientregistration_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./registration/patientregistration/patientregistration.component */ "./src/app/registration/patientregistration/patientregistration.component.ts");
/* harmony import */ var _registration_doctorregistration_doctorregistration_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./registration/doctorregistration/doctorregistration.component */ "./src/app/registration/doctorregistration/doctorregistration.component.ts");
/* harmony import */ var _patient_patient_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./patient/patient.module */ "./src/app/patient/patient.module.ts");
/* harmony import */ var _doctor_doctor_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./doctor/doctor.module */ "./src/app/doctor/doctor.module.ts");
/* harmony import */ var _authorization_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./authorization.service */ "./src/app/authorization.service.ts");
/* harmony import */ var _doctors_doctors_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./doctors/doctors.component */ "./src/app/doctors/doctors.component.ts");
/* harmony import */ var _search_pipe__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./search.pipe */ "./src/app/search.pipe.ts");
/* harmony import */ var _nav_otp_otp_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./nav/otp/otp.component */ "./src/app/nav/otp/otp.component.ts");
/* harmony import */ var _nav_changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./nav/changepassword/changepassword.component */ "./src/app/nav/changepassword/changepassword.component.ts");
/* harmony import */ var _nav_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./nav/forgotpassword/forgotpassword.component */ "./src/app/nav/forgotpassword/forgotpassword.component.ts");






















let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
            _nav_nav_component__WEBPACK_IMPORTED_MODULE_7__["NavComponent"],
            _home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
            _aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_9__["AboutusComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_10__["LoginComponent"],
            _registration_registration_component__WEBPACK_IMPORTED_MODULE_11__["RegistrationComponent"],
            _registration_patientregistration_patientregistration_component__WEBPACK_IMPORTED_MODULE_12__["PatientregistrationComponent"],
            _registration_doctorregistration_doctorregistration_component__WEBPACK_IMPORTED_MODULE_13__["DoctorregistrationComponent"],
            _doctors_doctors_component__WEBPACK_IMPORTED_MODULE_17__["DoctorsComponent"],
            _search_pipe__WEBPACK_IMPORTED_MODULE_18__["SearchPipe"],
            _nav_otp_otp_component__WEBPACK_IMPORTED_MODULE_19__["OtpComponent"],
            _nav_changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_20__["ChangepasswordComponent"],
            _nav_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_21__["ForgotpasswordComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
            _patient_patient_module__WEBPACK_IMPORTED_MODULE_14__["PatientModule"],
            _doctor_doctor_module__WEBPACK_IMPORTED_MODULE_15__["DoctorModule"]
        ],
        providers: [{
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HTTP_INTERCEPTORS"],
                useClass: _authorization_service__WEBPACK_IMPORTED_MODULE_16__["AuthorizationService"],
                multi: true
            }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/authorization.service.ts":
/*!******************************************!*\
  !*** ./src/app/authorization.service.ts ***!
  \******************************************/
/*! exports provided: AuthorizationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorizationService", function() { return AuthorizationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AuthorizationService = class AuthorizationService {
    constructor() { }
    intercept(req, next) {
        //read token from localstorage
        const idToken = localStorage.getItem("idToken");
        //if token is found ,add it to the header of request Object
        if (idToken) {
            const cloned = req.clone({ headers: req.headers.set("Authorization", "Bearer " + idToken) });
            //and then forward the request object cloned with token
            return next.handle(cloned);
        }
        else {
            //if token not found ,forward the same req object
            return next.handle(req);
        }
    }
};
AuthorizationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthorizationService);



/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/doctor-dashboard-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/doctor-dashboard-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: DoctorDashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorDashboardRoutingModule", function() { return DoctorDashboardRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _doctor_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./doctor-dashboard.component */ "./src/app/doctor/doctor-dashboard/doctor-dashboard.component.ts");
/* harmony import */ var _doctorprofile_doctorprofile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./doctorprofile/doctorprofile.component */ "./src/app/doctor/doctor-dashboard/doctorprofile/doctorprofile.component.ts");
/* harmony import */ var _viewrequests_viewrequests_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./viewrequests/viewrequests.component */ "./src/app/doctor/doctor-dashboard/viewrequests/viewrequests.component.ts");
/* harmony import */ var _paymentstatus_paymentstatus_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./paymentstatus/paymentstatus.component */ "./src/app/doctor/doctor-dashboard/paymentstatus/paymentstatus.component.ts");
/* harmony import */ var _myappointments_myappointments_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./myappointments/myappointments.component */ "./src/app/doctor/doctor-dashboard/myappointments/myappointments.component.ts");








const routes = [
    {
        path: 'doctordashboard',
        component: _doctor_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DoctorDashboardComponent"],
        children: [{
                path: 'profile',
                component: _doctorprofile_doctorprofile_component__WEBPACK_IMPORTED_MODULE_4__["DoctorprofileComponent"],
            },
            {
                path: 'viewrequests',
                component: _viewrequests_viewrequests_component__WEBPACK_IMPORTED_MODULE_5__["ViewrequestsComponent"]
            },
            {
                path: 'myappointments',
                component: _myappointments_myappointments_component__WEBPACK_IMPORTED_MODULE_7__["MyappointmentsComponent"]
            },
            {
                path: 'paymentstatus',
                component: _paymentstatus_paymentstatus_component__WEBPACK_IMPORTED_MODULE_6__["PaymentstatusComponent"]
            }
        ]
    },
];
let DoctorDashboardRoutingModule = class DoctorDashboardRoutingModule {
};
DoctorDashboardRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], DoctorDashboardRoutingModule);



/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/doctor-dashboard.component.css":
/*!************************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/doctor-dashboard.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RvY3Rvci9kb2N0b3ItZGFzaGJvYXJkL2RvY3Rvci1kYXNoYm9hcmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/doctor-dashboard.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/doctor-dashboard.component.ts ***!
  \***********************************************************************/
/*! exports provided: DoctorDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorDashboardComponent", function() { return DoctorDashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let DoctorDashboardComponent = class DoctorDashboardComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    logout() {
        localStorage.removeItem('idToken');
        this.router.navigate(['nav/home']);
    }
};
DoctorDashboardComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
DoctorDashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-doctor-dashboard',
        template: __webpack_require__(/*! raw-loader!./doctor-dashboard.component.html */ "./node_modules/raw-loader/index.js!./src/app/doctor/doctor-dashboard/doctor-dashboard.component.html"),
        styles: [__webpack_require__(/*! ./doctor-dashboard.component.css */ "./src/app/doctor/doctor-dashboard/doctor-dashboard.component.css")]
    })
], DoctorDashboardComponent);



/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/doctor-dashboard.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/doctor-dashboard.module.ts ***!
  \********************************************************************/
/*! exports provided: DoctorDashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorDashboardModule", function() { return DoctorDashboardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _doctor_dashboard_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./doctor-dashboard-routing.module */ "./src/app/doctor/doctor-dashboard/doctor-dashboard-routing.module.ts");
/* harmony import */ var _doctor_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./doctor-dashboard.component */ "./src/app/doctor/doctor-dashboard/doctor-dashboard.component.ts");
/* harmony import */ var _doctorprofile_doctorprofile_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./doctorprofile/doctorprofile.component */ "./src/app/doctor/doctor-dashboard/doctorprofile/doctorprofile.component.ts");
/* harmony import */ var _viewrequests_viewrequests_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./viewrequests/viewrequests.component */ "./src/app/doctor/doctor-dashboard/viewrequests/viewrequests.component.ts");
/* harmony import */ var _search_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./search.pipe */ "./src/app/doctor/doctor-dashboard/search.pipe.ts");
/* harmony import */ var _paymentstatus_paymentstatus_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./paymentstatus/paymentstatus.component */ "./src/app/doctor/doctor-dashboard/paymentstatus/paymentstatus.component.ts");
/* harmony import */ var _myappointments_myappointments_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./myappointments/myappointments.component */ "./src/app/doctor/doctor-dashboard/myappointments/myappointments.component.ts");











let DoctorDashboardModule = class DoctorDashboardModule {
};
DoctorDashboardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_doctor_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DoctorDashboardComponent"], _doctorprofile_doctorprofile_component__WEBPACK_IMPORTED_MODULE_6__["DoctorprofileComponent"], _viewrequests_viewrequests_component__WEBPACK_IMPORTED_MODULE_7__["ViewrequestsComponent"], _search_pipe__WEBPACK_IMPORTED_MODULE_8__["SearchPipe"], _paymentstatus_paymentstatus_component__WEBPACK_IMPORTED_MODULE_9__["PaymentstatusComponent"], _myappointments_myappointments_component__WEBPACK_IMPORTED_MODULE_10__["MyappointmentsComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _doctor_dashboard_routing_module__WEBPACK_IMPORTED_MODULE_4__["DoctorDashboardRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
        ]
    })
], DoctorDashboardModule);



/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/doctorprofile/doctorprofile.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/doctorprofile/doctorprofile.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RvY3Rvci9kb2N0b3ItZGFzaGJvYXJkL2RvY3RvcnByb2ZpbGUvZG9jdG9ycHJvZmlsZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/doctorprofile/doctorprofile.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/doctorprofile/doctorprofile.component.ts ***!
  \**********************************************************************************/
/*! exports provided: DoctorprofileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorprofileComponent", function() { return DoctorprofileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let DoctorprofileComponent = class DoctorprofileComponent {
    constructor(ts, hc, router) {
        this.ts = ts;
        this.hc = hc;
        this.router = router;
        this.currectUser = this.ts.currentUsername[0].name;
        this.b = true;
    }
    ngOnInit() {
        //console.log(this.register.currentUsername)
        this.hc.get(`doctordashboard/profile/${this.currectUser}`).subscribe(res => {
            if (res['message'] == "session expired") {
                alert("session expired.....! Please relogin");
                this.router.navigate(['nav/login']);
            }
            this.currentUser = res['data'];
        });
    }
    edit(data) {
        this.objectToUpdate = data;
        console.log(this.objectToUpdate);
        this.b = false;
    }
    submitEditData(modifiedData) {
        this.hc.put('doctordashboard/profile', modifiedData).subscribe(res => {
            alert(res["message"]);
        });
        this.b = true;
    }
};
DoctorprofileComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
DoctorprofileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-doctorprofile',
        template: __webpack_require__(/*! raw-loader!./doctorprofile.component.html */ "./node_modules/raw-loader/index.js!./src/app/doctor/doctor-dashboard/doctorprofile/doctorprofile.component.html"),
        styles: [__webpack_require__(/*! ./doctorprofile.component.css */ "./src/app/doctor/doctor-dashboard/doctorprofile/doctorprofile.component.css")]
    })
], DoctorprofileComponent);



/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/myappointments/myappointments.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/myappointments/myappointments.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RvY3Rvci9kb2N0b3ItZGFzaGJvYXJkL215YXBwb2ludG1lbnRzL215YXBwb2ludG1lbnRzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/myappointments/myappointments.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/myappointments/myappointments.component.ts ***!
  \************************************************************************************/
/*! exports provided: MyappointmentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyappointmentsComponent", function() { return MyappointmentsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let MyappointmentsComponent = class MyappointmentsComponent {
    constructor(hc, ts, router) {
        this.hc = hc;
        this.ts = ts;
        this.router = router;
    }
    ngOnInit() {
        this.hc.get(`/doctordashboard/myappointments/${this.ts.currentUsername[0].name}`).subscribe(res => {
            if (res['message'] == "session expired") {
                alert("session expired.....! Please relogin");
                this.router.navigate(['nav/login']);
            }
            this.
                bookings = res['message'];
        });
    }
};
MyappointmentsComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_3__["TransferService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
MyappointmentsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-myappointments',
        template: __webpack_require__(/*! raw-loader!./myappointments.component.html */ "./node_modules/raw-loader/index.js!./src/app/doctor/doctor-dashboard/myappointments/myappointments.component.html"),
        styles: [__webpack_require__(/*! ./myappointments.component.css */ "./src/app/doctor/doctor-dashboard/myappointments/myappointments.component.css")]
    })
], MyappointmentsComponent);



/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/paymentstatus/paymentstatus.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/paymentstatus/paymentstatus.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RvY3Rvci9kb2N0b3ItZGFzaGJvYXJkL3BheW1lbnRzdGF0dXMvcGF5bWVudHN0YXR1cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/paymentstatus/paymentstatus.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/paymentstatus/paymentstatus.component.ts ***!
  \**********************************************************************************/
/*! exports provided: PaymentstatusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentstatusComponent", function() { return PaymentstatusComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let PaymentstatusComponent = class PaymentstatusComponent {
    constructor(hc, ts, router) {
        this.hc = hc;
        this.ts = ts;
        this.router = router;
    }
    ngOnInit() {
        this.hc.get(`/doctordashboard/paymentstatus/${this.ts.currentUsername[0].name}`).subscribe(res => {
            if (res['message'] == "session expired") {
                alert("session expired.....! Please relogin");
                this.router.navigate(['nav/login']);
            }
            this.paymentData = res['data'];
        });
    }
};
PaymentstatusComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_3__["TransferService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
PaymentstatusComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-paymentstatus',
        template: __webpack_require__(/*! raw-loader!./paymentstatus.component.html */ "./node_modules/raw-loader/index.js!./src/app/doctor/doctor-dashboard/paymentstatus/paymentstatus.component.html"),
        styles: [__webpack_require__(/*! ./paymentstatus.component.css */ "./src/app/doctor/doctor-dashboard/paymentstatus/paymentstatus.component.css")]
    })
], PaymentstatusComponent);



/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/search.pipe.ts":
/*!********************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/search.pipe.ts ***!
  \********************************************************/
/*! exports provided: SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPipe", function() { return SearchPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SearchPipe = class SearchPipe {
    transform(value, ...args) {
        return null;
    }
};
SearchPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'search'
    })
], SearchPipe);



/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/viewrequests/viewrequests.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/viewrequests/viewrequests.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RvY3Rvci9kb2N0b3ItZGFzaGJvYXJkL3ZpZXdyZXF1ZXN0cy92aWV3cmVxdWVzdHMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/doctor/doctor-dashboard/viewrequests/viewrequests.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/doctor/doctor-dashboard/viewrequests/viewrequests.component.ts ***!
  \********************************************************************************/
/*! exports provided: ViewrequestsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewrequestsComponent", function() { return ViewrequestsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let ViewrequestsComponent = class ViewrequestsComponent {
    constructor(hc, ts, router) {
        this.hc = hc;
        this.ts = ts;
        this.router = router;
        this.patients = [];
    }
    ngOnInit() {
        this.hc.get(`/doctordashboard/viewrequests/${this.ts.currentUsername[0].name}`).subscribe(res => {
            if (res["message"] == "unauthorizated access") {
                alert(res["message"]);
            }
            else {
                if (res['message'] == "session expired") {
                    alert("session expired.....! Please relogin");
                    this.router.navigate(['nav/login']);
                }
            }
            this.patients = res['message'];
        });
        this.currentUser = this.ts.currentUsername;
    }
    accept(data, appdate, apptime) {
        data.reqstatus = "request accepted";
        this.ts.setResponse(data).subscribe(res => {
            //alert(res['message'])
        });
        var acceptedrequests = {
            "appdate": appdate,
            "apptime": apptime,
            "patientname": data.patientname,
            "patientnumber": data.patientnumber,
            "patientemail": data.patientemail,
            "patientarea": data.patientarea,
            "doctorname": this.currentUser[0].name,
            "doctornumber": this.currentUser[0].number,
            "doctoremail": this.currentUser[0].email,
            "doctorarea": this.currentUser[0].area,
            "doctorspec": this.currentUser[0].specialization,
            "doctorexp": this.currentUser[0].experience
        };
        this.hc.post('/doctordashboard/viewrequests', acceptedrequests).subscribe((res) => {
            alert(res['message']);
            // this.patients=res["data"]
        });
    }
    reject(data) {
        data.reqstatus = "request rejected";
        this.ts.setResponse(data).subscribe(res => {
            alert(res['message']);
        });
    }
};
ViewrequestsComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
ViewrequestsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-viewrequests',
        template: __webpack_require__(/*! raw-loader!./viewrequests.component.html */ "./node_modules/raw-loader/index.js!./src/app/doctor/doctor-dashboard/viewrequests/viewrequests.component.html"),
        styles: [__webpack_require__(/*! ./viewrequests.component.css */ "./src/app/doctor/doctor-dashboard/viewrequests/viewrequests.component.css")]
    })
], ViewrequestsComponent);



/***/ }),

/***/ "./src/app/doctor/doctor-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/doctor/doctor-routing.module.ts ***!
  \*************************************************/
/*! exports provided: DoctorRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorRoutingModule", function() { return DoctorRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [];
let DoctorRoutingModule = class DoctorRoutingModule {
};
DoctorRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], DoctorRoutingModule);



/***/ }),

/***/ "./src/app/doctor/doctor.module.ts":
/*!*****************************************!*\
  !*** ./src/app/doctor/doctor.module.ts ***!
  \*****************************************/
/*! exports provided: DoctorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorModule", function() { return DoctorModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _doctor_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./doctor-routing.module */ "./src/app/doctor/doctor-routing.module.ts");
/* harmony import */ var _doctor_dashboard_doctor_dashboard_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./doctor-dashboard/doctor-dashboard.module */ "./src/app/doctor/doctor-dashboard/doctor-dashboard.module.ts");





let DoctorModule = class DoctorModule {
};
DoctorModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _doctor_routing_module__WEBPACK_IMPORTED_MODULE_3__["DoctorRoutingModule"],
            _doctor_dashboard_doctor_dashboard_module__WEBPACK_IMPORTED_MODULE_4__["DoctorDashboardModule"]
        ]
    })
], DoctorModule);



/***/ }),

/***/ "./src/app/doctors/doctors.component.css":
/*!***********************************************!*\
  !*** ./src/app/doctors/doctors.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RvY3RvcnMvZG9jdG9ycy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/doctors/doctors.component.ts":
/*!**********************************************!*\
  !*** ./src/app/doctors/doctors.component.ts ***!
  \**********************************************/
/*! exports provided: DoctorsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorsComponent", function() { return DoctorsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let DoctorsComponent = class DoctorsComponent {
    constructor(hc) {
        this.hc = hc;
    }
    ngOnInit() {
        this.hc.get("nav/doctors").subscribe(res => {
            this.list = res['message'];
            console.log(this.list);
        });
    }
};
DoctorsComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
DoctorsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-doctors',
        template: __webpack_require__(/*! raw-loader!./doctors.component.html */ "./node_modules/raw-loader/index.js!./src/app/doctors/doctors.component.html"),
        styles: [__webpack_require__(/*! ./doctors.component.css */ "./src/app/doctors/doctors.component.css")]
    })
], DoctorsComponent);



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media only screen and (max-width: 500px){\r\n    .carousel-item img{\r\n          width:100%;\r\n          height:500px;\r\n    }\r\n\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSTtVQUNNLFVBQVU7VUFDVixZQUFZO0lBQ2xCOztBQUVKIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KXtcclxuICAgIC5jYXJvdXNlbC1pdGVtIGltZ3tcclxuICAgICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgICBoZWlnaHQ6NTAwcHg7XHJcbiAgICB9XHJcblxyXG59Il19 */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() { }
    ngOnInit() {
    }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
    })
], HomeComponent);



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "form{\r\n    border:2px solid rgb(19, 145, 248);\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGtDQUFrQztBQUN0QyIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJmb3Jte1xyXG4gICAgYm9yZGVyOjJweCBzb2xpZCByZ2IoMTksIDE0NSwgMjQ4KTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _transfer_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../transfer.service */ "./src/app/transfer.service.ts");





let LoginComponent = class LoginComponent {
    constructor(router, hc, ts) {
        this.router = router;
        this.hc = hc;
        this.ts = ts;
    }
    ngOnInit() {
    }
    submit(data) {
        this.hc.post('nav/login/', data).subscribe((res) => {
            if (res["message"] == 'patient name invalid') {
                alert('please valid patient name');
            }
            else if (res["message"] == 'patient password invalid') {
                alert('please enter valid patient password');
            }
            else if (res["message"] == 'patient logged in successfully') {
                alert('successfully logged in as patient');
                this.ts.currentUsername = res["userdata"];
                localStorage.setItem("idToken", res['token']);
                //console.log(res['token'])
                this.router.navigate(['/patientdashboard/profile']);
            }
            else if (res["message"] == 'doctor name invalid') {
                alert('please valid doctor name');
            }
            else if (res["message"] == 'doctor password invalid') {
                alert('please enter valid doctor password');
            }
            else if (res["message"] == 'doctor logged in successfully') {
                alert('successfully logged in as doctor');
                this.ts.currentUsername = res["userdata"];
                localStorage.setItem("idToken", res['token']);
                //console.log(res['token'])
                this.router.navigate(['/doctordashboard/profile']);
            }
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _transfer_service__WEBPACK_IMPORTED_MODULE_4__["TransferService"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/nav/changepassword/changepassword.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/nav/changepassword/changepassword.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hdi9jaGFuZ2VwYXNzd29yZC9jaGFuZ2VwYXNzd29yZC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/nav/changepassword/changepassword.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/nav/changepassword/changepassword.component.ts ***!
  \****************************************************************/
/*! exports provided: ChangepasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangepasswordComponent", function() { return ChangepasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let ChangepasswordComponent = class ChangepasswordComponent {
    constructor(http, route) {
        this.http = http;
        this.route = route;
    }
    ngOnInit() {
    }
    changepassword(y) {
        this.http.put("/nav/changepassword", y).subscribe(res => {
            if (res["message"] == "password changed") {
                alert(res["message"]);
                this.route.navigate(["/nav/login"]);
            }
            else {
                this.route.navigate(["/nav/otp"]);
            }
        });
    }
};
ChangepasswordComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
ChangepasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-changepassword',
        template: __webpack_require__(/*! raw-loader!./changepassword.component.html */ "./node_modules/raw-loader/index.js!./src/app/nav/changepassword/changepassword.component.html"),
        styles: [__webpack_require__(/*! ./changepassword.component.css */ "./src/app/nav/changepassword/changepassword.component.css")]
    })
], ChangepasswordComponent);



/***/ }),

/***/ "./src/app/nav/forgotpassword/forgotpassword.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/nav/forgotpassword/forgotpassword.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hdi9mb3Jnb3RwYXNzd29yZC9mb3Jnb3RwYXNzd29yZC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/nav/forgotpassword/forgotpassword.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/nav/forgotpassword/forgotpassword.component.ts ***!
  \****************************************************************/
/*! exports provided: ForgotpasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotpasswordComponent", function() { return ForgotpasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let ForgotpasswordComponent = class ForgotpasswordComponent {
    constructor(http, route) {
        this.http = http;
        this.route = route;
    }
    ngOnInit() {
    }
    password(x) {
        this.http.post('nav/forgotpassword', x).subscribe(res => {
            alert(res["message"]);
            if (res["message"] == "user found") {
                this.route.navigate(['nav/otp']);
            }
            else {
                this.route.navigate(['nav/forgotpassword']);
            }
        });
    }
};
ForgotpasswordComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
ForgotpasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-forgotpassword',
        template: __webpack_require__(/*! raw-loader!./forgotpassword.component.html */ "./node_modules/raw-loader/index.js!./src/app/nav/forgotpassword/forgotpassword.component.html"),
        styles: [__webpack_require__(/*! ./forgotpassword.component.css */ "./src/app/nav/forgotpassword/forgotpassword.component.css")]
    })
], ForgotpasswordComponent);



/***/ }),

/***/ "./src/app/nav/nav.component.css":
/*!***************************************!*\
  !*** ./src/app/nav/nav.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media only screen and (max-width: 500px){\r\n    .hide{\r\n        display:none;\r\n    }\r\n    .nav-item a{\r\n        margin-bottom:5px;\r\n    }\r\n    \r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmF2L25hdi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0k7UUFDSSxZQUFZO0lBQ2hCO0lBQ0E7UUFDSSxpQkFBaUI7SUFDckI7O0FBRUoiLCJmaWxlIjoic3JjL2FwcC9uYXYvbmF2LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KXtcclxuICAgIC5oaWRle1xyXG4gICAgICAgIGRpc3BsYXk6bm9uZTtcclxuICAgIH1cclxuICAgIC5uYXYtaXRlbSBhe1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206NXB4O1xyXG4gICAgfVxyXG4gICAgXHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/nav/nav.component.ts":
/*!**************************************!*\
  !*** ./src/app/nav/nav.component.ts ***!
  \**************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NavComponent = class NavComponent {
    constructor() { }
    ngOnInit() {
    }
};
NavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-nav',
        template: __webpack_require__(/*! raw-loader!./nav.component.html */ "./node_modules/raw-loader/index.js!./src/app/nav/nav.component.html"),
        styles: [__webpack_require__(/*! ./nav.component.css */ "./src/app/nav/nav.component.css")]
    })
], NavComponent);



/***/ }),

/***/ "./src/app/nav/otp/otp.component.css":
/*!*******************************************!*\
  !*** ./src/app/nav/otp/otp.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hdi9vdHAvb3RwLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/nav/otp/otp.component.ts":
/*!******************************************!*\
  !*** ./src/app/nav/otp/otp.component.ts ***!
  \******************************************/
/*! exports provided: OtpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpComponent", function() { return OtpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let OtpComponent = class OtpComponent {
    constructor(route, http) {
        this.route = route;
        this.http = http;
    }
    ngOnInit() {
    }
    otp(x) {
        this.http.post('nav/otp', x).subscribe(res => {
            if (res["message"] == "verifiedOTP") {
                this.route.navigate(["/nav/changepassword"]);
            }
            else {
                alert(res["message"]);
                this.route.navigate(["/nav/otp/"]);
            }
        });
    }
};
OtpComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
OtpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-otp',
        template: __webpack_require__(/*! raw-loader!./otp.component.html */ "./node_modules/raw-loader/index.js!./src/app/nav/otp/otp.component.html"),
        styles: [__webpack_require__(/*! ./otp.component.css */ "./src/app/nav/otp/otp.component.css")]
    })
], OtpComponent);



/***/ }),

/***/ "./src/app/patient/patient-dashboard/makepayment/makepayment.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/makepayment/makepayment.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "span{\r\n    color:red;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF0aWVudC9wYXRpZW50LWRhc2hib2FyZC9tYWtlcGF5bWVudC9tYWtlcGF5bWVudC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksU0FBUztBQUNiIiwiZmlsZSI6InNyYy9hcHAvcGF0aWVudC9wYXRpZW50LWRhc2hib2FyZC9tYWtlcGF5bWVudC9tYWtlcGF5bWVudC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsic3BhbntcclxuICAgIGNvbG9yOnJlZDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/patient/patient-dashboard/makepayment/makepayment.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/makepayment/makepayment.component.ts ***!
  \********************************************************************************/
/*! exports provided: MakepaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MakepaymentComponent", function() { return MakepaymentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let MakepaymentComponent = class MakepaymentComponent {
    constructor(ts, hc, router) {
        this.ts = ts;
        this.hc = hc;
        this.router = router;
    }
    ngOnInit() {
    }
    payment(data) {
        console.log(data);
        data.patientname = this.ts.currentUsername[0].name;
        data.paystatus = 'paid';
        this.hc.post('/patientdashboard/makepayment', data).subscribe(res => {
            if (res["message"] == "null values not inserted") {
                alert("please fill all required fields");
            }
            else if (res['message'] == "session expired") {
                alert("session expired.....! Please relogin");
                this.router.navigate(['nav/login']);
            }
            else {
                alert(res["message"]);
            }
        });
    }
};
MakepaymentComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
MakepaymentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-makepayment',
        template: __webpack_require__(/*! raw-loader!./makepayment.component.html */ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/makepayment/makepayment.component.html"),
        styles: [__webpack_require__(/*! ./makepayment.component.css */ "./src/app/patient/patient-dashboard/makepayment/makepayment.component.css")]
    })
], MakepaymentComponent);



/***/ }),

/***/ "./src/app/patient/patient-dashboard/mybookings/mybookings.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/mybookings/mybookings.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhdGllbnQvcGF0aWVudC1kYXNoYm9hcmQvbXlib29raW5ncy9teWJvb2tpbmdzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/patient/patient-dashboard/mybookings/mybookings.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/mybookings/mybookings.component.ts ***!
  \******************************************************************************/
/*! exports provided: MybookingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MybookingsComponent", function() { return MybookingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");




let MybookingsComponent = class MybookingsComponent {
    constructor(hc, ts) {
        this.hc = hc;
        this.ts = ts;
    }
    ngOnInit() {
        this.hc.get(`/patientdashboard/mybookings/${this.ts.currentUsername[0].name}`).subscribe(res => {
            /*  if(res["message"]=="unauthorizated access"){
                alert(res["message"])
              }*/
            this.
                bookings = res['message'];
        });
    }
};
MybookingsComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_3__["TransferService"] }
];
MybookingsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-mybookings',
        template: __webpack_require__(/*! raw-loader!./mybookings.component.html */ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/mybookings/mybookings.component.html"),
        styles: [__webpack_require__(/*! ./mybookings.component.css */ "./src/app/patient/patient-dashboard/mybookings/mybookings.component.css")]
    })
], MybookingsComponent);



/***/ }),

/***/ "./src/app/patient/patient-dashboard/patient-dashboard-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/patient-dashboard-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: PatientDashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientDashboardRoutingModule", function() { return PatientDashboardRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _patient_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./patient-dashboard.component */ "./src/app/patient/patient-dashboard/patient-dashboard.component.ts");
/* harmony import */ var _patientprofile_patientprofile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./patientprofile/patientprofile.component */ "./src/app/patient/patient-dashboard/patientprofile/patientprofile.component.ts");
/* harmony import */ var _viewdoctors_viewdoctors_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./viewdoctors/viewdoctors.component */ "./src/app/patient/patient-dashboard/viewdoctors/viewdoctors.component.ts");
/* harmony import */ var _makepayment_makepayment_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./makepayment/makepayment.component */ "./src/app/patient/patient-dashboard/makepayment/makepayment.component.ts");
/* harmony import */ var _paymenthistory_paymenthistory_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./paymenthistory/paymenthistory.component */ "./src/app/patient/patient-dashboard/paymenthistory/paymenthistory.component.ts");
/* harmony import */ var _mybookings_mybookings_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./mybookings/mybookings.component */ "./src/app/patient/patient-dashboard/mybookings/mybookings.component.ts");









const routes = [
    {
        path: 'patientdashboard',
        component: _patient_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["PatientDashboardComponent"],
        children: [{
                path: 'profile',
                component: _patientprofile_patientprofile_component__WEBPACK_IMPORTED_MODULE_4__["PatientprofileComponent"],
            },
            {
                path: 'viewdoctors',
                component: _viewdoctors_viewdoctors_component__WEBPACK_IMPORTED_MODULE_5__["ViewdoctorsComponent"]
            },
            {
                path: 'mybookings',
                component: _mybookings_mybookings_component__WEBPACK_IMPORTED_MODULE_8__["MybookingsComponent"]
            },
            {
                path: 'makepayment',
                component: _makepayment_makepayment_component__WEBPACK_IMPORTED_MODULE_6__["MakepaymentComponent"]
            },
            {
                path: 'paymenthistory',
                component: _paymenthistory_paymenthistory_component__WEBPACK_IMPORTED_MODULE_7__["PaymenthistoryComponent"]
            }
        ]
    },
];
let PatientDashboardRoutingModule = class PatientDashboardRoutingModule {
};
PatientDashboardRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PatientDashboardRoutingModule);



/***/ }),

/***/ "./src/app/patient/patient-dashboard/patient-dashboard.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/patient-dashboard.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhdGllbnQvcGF0aWVudC1kYXNoYm9hcmQvcGF0aWVudC1kYXNoYm9hcmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/patient/patient-dashboard/patient-dashboard.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/patient-dashboard.component.ts ***!
  \**************************************************************************/
/*! exports provided: PatientDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientDashboardComponent", function() { return PatientDashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let PatientDashboardComponent = class PatientDashboardComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    logout() {
        localStorage.removeItem('idToken');
        this.router.navigate(['nav/home']);
    }
};
PatientDashboardComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
PatientDashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-patient-dashboard',
        template: __webpack_require__(/*! raw-loader!./patient-dashboard.component.html */ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/patient-dashboard.component.html"),
        styles: [__webpack_require__(/*! ./patient-dashboard.component.css */ "./src/app/patient/patient-dashboard/patient-dashboard.component.css")]
    })
], PatientDashboardComponent);



/***/ }),

/***/ "./src/app/patient/patient-dashboard/patient-dashboard.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/patient-dashboard.module.ts ***!
  \***********************************************************************/
/*! exports provided: PatientDashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientDashboardModule", function() { return PatientDashboardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _patient_dashboard_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./patient-dashboard-routing.module */ "./src/app/patient/patient-dashboard/patient-dashboard-routing.module.ts");
/* harmony import */ var _patient_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./patient-dashboard.component */ "./src/app/patient/patient-dashboard/patient-dashboard.component.ts");
/* harmony import */ var _patientprofile_patientprofile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./patientprofile/patientprofile.component */ "./src/app/patient/patient-dashboard/patientprofile/patientprofile.component.ts");
/* harmony import */ var _viewdoctors_viewdoctors_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./viewdoctors/viewdoctors.component */ "./src/app/patient/patient-dashboard/viewdoctors/viewdoctors.component.ts");
/* harmony import */ var _search_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./search.pipe */ "./src/app/patient/patient-dashboard/search.pipe.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _makepayment_makepayment_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./makepayment/makepayment.component */ "./src/app/patient/patient-dashboard/makepayment/makepayment.component.ts");
/* harmony import */ var _paymenthistory_paymenthistory_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./paymenthistory/paymenthistory.component */ "./src/app/patient/patient-dashboard/paymenthistory/paymenthistory.component.ts");
/* harmony import */ var _mybookings_mybookings_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./mybookings/mybookings.component */ "./src/app/patient/patient-dashboard/mybookings/mybookings.component.ts");












let PatientDashboardModule = class PatientDashboardModule {
};
PatientDashboardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_patient_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["PatientDashboardComponent"], _patientprofile_patientprofile_component__WEBPACK_IMPORTED_MODULE_5__["PatientprofileComponent"], _viewdoctors_viewdoctors_component__WEBPACK_IMPORTED_MODULE_6__["ViewdoctorsComponent"], _search_pipe__WEBPACK_IMPORTED_MODULE_7__["SearchPipe"], _makepayment_makepayment_component__WEBPACK_IMPORTED_MODULE_9__["MakepaymentComponent"], _paymenthistory_paymenthistory_component__WEBPACK_IMPORTED_MODULE_10__["PaymenthistoryComponent"], _mybookings_mybookings_component__WEBPACK_IMPORTED_MODULE_11__["MybookingsComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _patient_dashboard_routing_module__WEBPACK_IMPORTED_MODULE_3__["PatientDashboardRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"]
        ]
    })
], PatientDashboardModule);



/***/ }),

/***/ "./src/app/patient/patient-dashboard/patientprofile/patientprofile.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/patientprofile/patientprofile.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhdGllbnQvcGF0aWVudC1kYXNoYm9hcmQvcGF0aWVudHByb2ZpbGUvcGF0aWVudHByb2ZpbGUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/patient/patient-dashboard/patientprofile/patientprofile.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/patientprofile/patientprofile.component.ts ***!
  \**************************************************************************************/
/*! exports provided: PatientprofileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientprofileComponent", function() { return PatientprofileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let PatientprofileComponent = class PatientprofileComponent {
    constructor(ts, hc, router) {
        this.ts = ts;
        this.hc = hc;
        this.router = router;
        this.b = true;
        this.currectUser = this.ts.currentUsername[0].name;
    }
    ngOnInit() {
        //console.log(this.register.currentUsername)
        this.hc.get(`patientdashboard/profile/${this.currectUser}`).subscribe(res => {
            if (res['message'] == "session expired") {
                alert("session expired.....! Please relogin");
                this.router.navigate(['nav/login']);
            }
            this.currentUser = res['data'];
        });
    }
    edit(data) {
        this.objectToUpdate = data;
        console.log(this.objectToUpdate);
        this.b = false;
    }
    submitEditData(modifiedData) {
        this.hc.put('patientdashboard/profile', modifiedData).subscribe(res => {
            alert(res["message"]);
        });
        this.b = true;
    }
};
PatientprofileComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
PatientprofileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-patientprofile',
        template: __webpack_require__(/*! raw-loader!./patientprofile.component.html */ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/patientprofile/patientprofile.component.html"),
        styles: [__webpack_require__(/*! ./patientprofile.component.css */ "./src/app/patient/patient-dashboard/patientprofile/patientprofile.component.css")]
    })
], PatientprofileComponent);



/***/ }),

/***/ "./src/app/patient/patient-dashboard/paymenthistory/paymenthistory.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/paymenthistory/paymenthistory.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhdGllbnQvcGF0aWVudC1kYXNoYm9hcmQvcGF5bWVudGhpc3RvcnkvcGF5bWVudGhpc3RvcnkuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/patient/patient-dashboard/paymenthistory/paymenthistory.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/paymenthistory/paymenthistory.component.ts ***!
  \**************************************************************************************/
/*! exports provided: PaymenthistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymenthistoryComponent", function() { return PaymenthistoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let PaymenthistoryComponent = class PaymenthistoryComponent {
    constructor(ts, hc, router) {
        this.ts = ts;
        this.hc = hc;
        this.router = router;
    }
    ngOnInit() {
        this.hc.get(`/patientdashboard/paymenthistory/${this.ts.currentUsername[0].name}`).subscribe(res => {
            if (res['message'] == "session expired") {
                alert("session expired.....! Please relogin");
                this.router.navigate(['nav/login']);
            }
            this.paymentData = res['data'];
        });
    }
};
PaymenthistoryComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
PaymenthistoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-paymenthistory',
        template: __webpack_require__(/*! raw-loader!./paymenthistory.component.html */ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/paymenthistory/paymenthistory.component.html"),
        styles: [__webpack_require__(/*! ./paymenthistory.component.css */ "./src/app/patient/patient-dashboard/paymenthistory/paymenthistory.component.css")]
    })
], PaymenthistoryComponent);



/***/ }),

/***/ "./src/app/patient/patient-dashboard/search.pipe.ts":
/*!**********************************************************!*\
  !*** ./src/app/patient/patient-dashboard/search.pipe.ts ***!
  \**********************************************************/
/*! exports provided: SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPipe", function() { return SearchPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SearchPipe = class SearchPipe {
    transform(list, searchWord) {
        if (!searchWord) {
            return list;
        }
        return list.filter(obj => obj.name.toLowerCase().indexOf(searchWord.toLowerCase()) != -1 ||
            obj.area.toLowerCase().indexOf(searchWord.toLowerCase()) != -1 ||
            obj.specilization.toLowerCase().indexOf(searchWord.toLowerCase()) != -1);
    }
};
SearchPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'search'
    })
], SearchPipe);



/***/ }),

/***/ "./src/app/patient/patient-dashboard/viewdoctors/viewdoctors.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/viewdoctors/viewdoctors.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhdGllbnQvcGF0aWVudC1kYXNoYm9hcmQvdmlld2RvY3RvcnMvdmlld2RvY3RvcnMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/patient/patient-dashboard/viewdoctors/viewdoctors.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/patient/patient-dashboard/viewdoctors/viewdoctors.component.ts ***!
  \********************************************************************************/
/*! exports provided: ViewdoctorsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewdoctorsComponent", function() { return ViewdoctorsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let ViewdoctorsComponent = class ViewdoctorsComponent {
    constructor(hc, ts, router) {
        this.hc = hc;
        this.ts = ts;
        this.router = router;
        this.b = true;
        this.list = [];
    }
    ngOnInit() {
        this.currentUser = this.ts.currentUsername;
        this.hc.get("/patientdashboard/viewdoctors").subscribe(res => {
            if (res["message"] == "unauthorizated access") {
                alert(res["message"]);
            }
            else {
                if (res['message'] == "session expired") {
                    alert("session expired.....! Please relogin");
                    this.router.navigate(['nav/login']);
                }
            }
            this.list = res['message'];
            console.log(this.list);
        });
    }
    ngOnChanges() {
    }
    changeStatus(doctorObject) {
        console.log(doctorObject);
        console.log(this.currentUser);
        var bookappointment = {
            "reqstatus": '',
            "patientname": this.currentUser[0].name,
            "patientnumber": this.currentUser[0].mobileno,
            "patientemail": this.currentUser[0].email,
            "patientarea": this.currentUser[0].area,
            "doctorname": doctorObject.name,
            "doctornumber": doctorObject.number,
            "doctoremail": doctorObject.email,
            "doctorarea": doctorObject.area,
            "doctorspec": doctorObject.specilization,
            "doctorexp": doctorObject.experience
        };
        console.log(bookappointment);
        this.hc.post('/patientdashboard/viewdoctors', bookappointment).subscribe(res => {
            alert(res['message']);
        });
        // this.b=false;
        // this.s.valueFromWhomTolet=this.b;
    }
};
ViewdoctorsComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_3__["TransferService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
ViewdoctorsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-viewdoctors',
        template: __webpack_require__(/*! raw-loader!./viewdoctors.component.html */ "./node_modules/raw-loader/index.js!./src/app/patient/patient-dashboard/viewdoctors/viewdoctors.component.html"),
        styles: [__webpack_require__(/*! ./viewdoctors.component.css */ "./src/app/patient/patient-dashboard/viewdoctors/viewdoctors.component.css")]
    })
], ViewdoctorsComponent);



/***/ }),

/***/ "./src/app/patient/patient-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/patient/patient-routing.module.ts ***!
  \***************************************************/
/*! exports provided: PatientRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientRoutingModule", function() { return PatientRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [];
let PatientRoutingModule = class PatientRoutingModule {
};
PatientRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PatientRoutingModule);



/***/ }),

/***/ "./src/app/patient/patient.module.ts":
/*!*******************************************!*\
  !*** ./src/app/patient/patient.module.ts ***!
  \*******************************************/
/*! exports provided: PatientModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientModule", function() { return PatientModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _patient_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./patient-routing.module */ "./src/app/patient/patient-routing.module.ts");
/* harmony import */ var _patient_dashboard_patient_dashboard_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./patient-dashboard/patient-dashboard.module */ "./src/app/patient/patient-dashboard/patient-dashboard.module.ts");





let PatientModule = class PatientModule {
};
PatientModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _patient_routing_module__WEBPACK_IMPORTED_MODULE_3__["PatientRoutingModule"],
            _patient_dashboard_patient_dashboard_module__WEBPACK_IMPORTED_MODULE_4__["PatientDashboardModule"]
        ]
    })
], PatientModule);



/***/ }),

/***/ "./src/app/registration/doctorregistration/doctorregistration.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/registration/doctorregistration/doctorregistration.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table td{\r\n    border:none !important\r\n}\r\n\r\nlabel{\r\n    \r\n    font-weight:bold;\r\n}\r\n\r\nh3{\r\n    text-decoration: underline;\r\n}\r\n\r\nspan{\r\n    color:red;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0cmF0aW9uL2RvY3RvcnJlZ2lzdHJhdGlvbi9kb2N0b3JyZWdpc3RyYXRpb24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJO0FBQ0o7O0FBRUE7O0lBRUksZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksMEJBQTBCO0FBQzlCOztBQUNBO0lBQ0ksU0FBUztBQUNiIiwiZmlsZSI6InNyYy9hcHAvcmVnaXN0cmF0aW9uL2RvY3RvcnJlZ2lzdHJhdGlvbi9kb2N0b3JyZWdpc3RyYXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHRke1xyXG4gICAgYm9yZGVyOm5vbmUgIWltcG9ydGFudFxyXG59XHJcblxyXG5sYWJlbHtcclxuICAgIFxyXG4gICAgZm9udC13ZWlnaHQ6Ym9sZDtcclxufVxyXG5oM3tcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG59XHJcbnNwYW57XHJcbiAgICBjb2xvcjpyZWQ7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/registration/doctorregistration/doctorregistration.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/registration/doctorregistration/doctorregistration.component.ts ***!
  \*********************************************************************************/
/*! exports provided: DoctorregistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorregistrationComponent", function() { return DoctorregistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let DoctorregistrationComponent = class DoctorregistrationComponent {
    constructor(router, hc) {
        this.router = router;
        this.hc = hc;
        this.genders = ['male', 'female', 'others'];
        this.specilizations = ['Adolescent medicine specialist', 'Cardiologist', 'Dermatologist', 'Forensic pathologist', 'Gynecologist', 'Hospitalist', 'Neurologist', 'Pathologist', 'Radiologist', 'Surgeon'];
        this.areas = ['Ameerpet', 'Nalgonda', 'Kukatpally', 'ABIDS', 'Miyapur', 'Panjagutta', 'Erragadda', 'Kirathabad', 'Gachibowli', 'JNTU', 'Manikonda', 'Borabanda', 'HITech City'];
    }
    ngOnInit() {
    }
    doctorReg(data) {
        this.hc.post('nav/register/doctor', data).subscribe((res) => {
            if (res["message"] == "null values not inserted") {
                alert("please fill all required fields");
            }
            else if (res["message"] == "registered successfully") {
                alert(res["message"]);
                this.router.navigate(['nav/login/']);
            }
            else if (res["message"] == "Duplicate Username") {
                alert('username already exists, please choose another');
            }
        });
    }
};
DoctorregistrationComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
DoctorregistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-doctorregistration',
        template: __webpack_require__(/*! raw-loader!./doctorregistration.component.html */ "./node_modules/raw-loader/index.js!./src/app/registration/doctorregistration/doctorregistration.component.html"),
        styles: [__webpack_require__(/*! ./doctorregistration.component.css */ "./src/app/registration/doctorregistration/doctorregistration.component.css")]
    })
], DoctorregistrationComponent);



/***/ }),

/***/ "./src/app/registration/patientregistration/patientregistration.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/registration/patientregistration/patientregistration.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table td{\r\n    border:none !important\r\n}\r\n\r\nlabel{\r\n    \r\n    font-weight:bold;\r\n}\r\n\r\nh3{\r\n    text-decoration: underline;\r\n}\r\n\r\nspan{\r\n    color:red;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0cmF0aW9uL3BhdGllbnRyZWdpc3RyYXRpb24vcGF0aWVudHJlZ2lzdHJhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0k7QUFDSjs7QUFFQTs7SUFFSSxnQkFBZ0I7QUFDcEI7O0FBQ0E7SUFDSSwwQkFBMEI7QUFDOUI7O0FBQ0E7SUFDSSxTQUFTO0FBQ2IiLCJmaWxlIjoic3JjL2FwcC9yZWdpc3RyYXRpb24vcGF0aWVudHJlZ2lzdHJhdGlvbi9wYXRpZW50cmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB0ZHtcclxuICAgIGJvcmRlcjpub25lICFpbXBvcnRhbnRcclxufVxyXG5cclxubGFiZWx7XHJcbiAgICBcclxuICAgIGZvbnQtd2VpZ2h0OmJvbGQ7XHJcbn1cclxuaDN7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxufVxyXG5zcGFue1xyXG4gICAgY29sb3I6cmVkO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/registration/patientregistration/patientregistration.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/registration/patientregistration/patientregistration.component.ts ***!
  \***********************************************************************************/
/*! exports provided: PatientregistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientregistrationComponent", function() { return PatientregistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let PatientregistrationComponent = class PatientregistrationComponent {
    constructor(router, hc) {
        this.router = router;
        this.hc = hc;
        this.areas = ['Ameerpet', 'Nalgonda', 'Kukatpally', 'ABIDS', 'Miyapur', 'Panjagutta', 'Erragadda', 'Kirathabad', 'Gachibowli', 'JNTU', 'Manikonda', 'Borabanda', 'HITech City'];
        this.genders = ['male', 'female', 'others'];
    }
    ngOnInit() {
    }
    patientReg(data) {
        this.hc.post('nav/register/patient', data).subscribe((res) => {
            if (res["message"] == "null values not inserted") {
                alert("please fill all required fields");
            }
            else if (res["message"] == "registered successfully") {
                alert(res["message"]);
                this.router.navigate(['nav/login/']);
            }
            else if (res["message"] == "duplicate Username") {
                alert('username already exists, please choose another');
            }
        });
    }
};
PatientregistrationComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
PatientregistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-patientregistration',
        template: __webpack_require__(/*! raw-loader!./patientregistration.component.html */ "./node_modules/raw-loader/index.js!./src/app/registration/patientregistration/patientregistration.component.html"),
        styles: [__webpack_require__(/*! ./patientregistration.component.css */ "./src/app/registration/patientregistration/patientregistration.component.css")]
    })
], PatientregistrationComponent);



/***/ }),

/***/ "./src/app/registration/registration.component.css":
/*!*********************************************************!*\
  !*** ./src/app/registration/registration.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdHJhdGlvbi9yZWdpc3RyYXRpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/registration/registration.component.ts":
/*!********************************************************!*\
  !*** ./src/app/registration/registration.component.ts ***!
  \********************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let RegistrationComponent = class RegistrationComponent {
    constructor() { }
    ngOnInit() {
    }
};
RegistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-registration',
        template: __webpack_require__(/*! raw-loader!./registration.component.html */ "./node_modules/raw-loader/index.js!./src/app/registration/registration.component.html"),
        styles: [__webpack_require__(/*! ./registration.component.css */ "./src/app/registration/registration.component.css")]
    })
], RegistrationComponent);



/***/ }),

/***/ "./src/app/search.pipe.ts":
/*!********************************!*\
  !*** ./src/app/search.pipe.ts ***!
  \********************************/
/*! exports provided: SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPipe", function() { return SearchPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SearchPipe = class SearchPipe {
    transform(data, searchTerm) {
        if (!searchTerm) {
            return data;
        }
        return data.filter(obj => obj.name.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1 ||
            obj.area.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1 ||
            obj.specilization.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1);
    }
};
SearchPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'search'
    })
], SearchPipe);



/***/ }),

/***/ "./src/app/transfer.service.ts":
/*!*************************************!*\
  !*** ./src/app/transfer.service.ts ***!
  \*************************************/
/*! exports provided: TransferService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferService", function() { return TransferService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let TransferService = class TransferService {
    constructor(hc) {
        this.hc = hc;
    }
    setResponse(data) {
        return this.hc.put('/doctordashboard/viewrequests', data);
    }
};
TransferService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
TransferService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], TransferService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\DoctorOnlineAppointment\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map