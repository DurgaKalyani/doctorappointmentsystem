import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(list: any[], searchWord: string): any {
    if (!searchWord)
    {
      return list;
    }
    return list.filter(obj=>
      obj.name.toLowerCase().indexOf(searchWord.toLowerCase())!=-1||
      obj.area.toLowerCase().indexOf(searchWord.toLowerCase())!=-1||
      obj.specilization.toLowerCase().indexOf(searchWord.toLowerCase())!=-1
      
    )
  }

}
