import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { observable } from 'rxjs';

@Component({
  selector: 'app-doctorregistration',
  templateUrl: './doctorregistration.component.html',
  styleUrls: ['./doctorregistration.component.css']
})
export class DoctorregistrationComponent implements OnInit {
  genders:string[]=['male','female','others'];
  specilizations:string[]=['Adolescent medicine specialist','Cardiologist','Dermatologist','Forensic pathologist','Gynecologist','Hospitalist','Neurologist','Pathologist','Radiologist','Surgeon']
  areas:string[]=['Ameerpet','Nalgonda','Kukatpally','ABIDS','Miyapur','Panjagutta','Erragadda','Kirathabad','Gachibowli','JNTU','Manikonda','Borabanda','HITech City']

  constructor(private router: Router,private hc:HttpClient) { }

  ngOnInit() {
  }
  doctorReg(data){
    this.hc.post('nav/register/doctor',data).subscribe((res)=>{
      if(res["message"]=="null values not inserted"){
        alert("please fill all required fields")
      }
      
     else if(res["message"]=="registered successfully")
      {
        alert(res["message"])
        this.router.navigate(['nav/login/'])
      }
      else if(res["message"]=="Duplicate Username")
      {
        alert('username already exists, please choose another')
      }
    })
   
  }
}
