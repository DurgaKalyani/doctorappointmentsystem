import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { observable } from 'rxjs';

@Component({
  selector: 'app-patientregistration',
  templateUrl: './patientregistration.component.html',
  styleUrls: ['./patientregistration.component.css']
})
export class PatientregistrationComponent implements OnInit {
  areas:string[]=['Ameerpet','Nalgonda','Kukatpally','ABIDS','Miyapur','Panjagutta','Erragadda','Kirathabad','Gachibowli','JNTU','Manikonda','Borabanda','HITech City']
  genders:string[]=['male','female','others']

  constructor(private router: Router,private hc:HttpClient) { }


  ngOnInit() {
  }

  patientReg(data){
    this.hc.post('nav/register/patient',data).subscribe((res)=>{
      if(res["message"]=="null values not inserted"){
        alert("please fill all required fields")
      }
      
     else if(res["message"]=="registered successfully")
      {
        alert(res["message"])
        this.router.navigate(['nav/login/'])
      }
      else if(res["message"]=="duplicate Username")
      {
        alert('username already exists, please choose another')
      }
    })
   
  }

}
