import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(data: any[], searchTerm: string): any {
    if(!searchTerm){
      return data;
    }
    return data.filter(obj=>
      obj.name.toLowerCase().indexOf(searchTerm.toLowerCase())!=-1||
         obj.area.toLowerCase().indexOf(searchTerm.toLowerCase())!=-1||
         obj.specilization.toLowerCase().indexOf(searchTerm.toLowerCase())!=-1
  
        );
    }
  

}
